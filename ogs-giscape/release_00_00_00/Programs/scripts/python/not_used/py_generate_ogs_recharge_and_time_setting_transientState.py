import sys
import os
from vtk import *

# Define input parameters from command line arguments
relative_input_file_path = str(sys.argv[1])
relative_xml_output_file_path_recharge = str(sys.argv[2])
relative_xml_output_file_path_timestepping = str(sys.argv[3])
file_path = os.getcwd()

# Generate full file paths for input and output files
vtu_file_name = file_path + '/' + relative_input_file_path
xml_file_name_recharge = file_path + '/' + relative_xml_output_file_path_recharge
xml_file_name_timestepping = file_path + '/' + relative_xml_output_file_path_timestepping

# Set simulation parameters
start_time = 0
timestep_size = 2628000  # Uniform timestep size in seconds

# Initialize VTK reader
reader = vtkXMLUnstructuredGridReader()
reader.SetFileName(vtu_file_name)
reader.Update()
mesh = reader.GetOutput()
all_attributes = mesh.GetAttributesAsFieldData(1)
band_counter = 0

# Count the number of bands in the VTK file
for i in range(all_attributes.GetNumberOfComponents()):
    name = all_attributes.GetArrayName(i)
    if name.find('Band') != -1:
        band_counter += 1

# Calculate simulation time in different units
sim_time_seconds = band_counter * timestep_size
sim_time_days = sim_time_seconds / 86400
sim_time_weeks = sim_time_seconds / (86400 * 7)
sim_time_months = sim_time_seconds / (86400 * 30.5)
sim_time_years = sim_time_seconds / (86400 * 365)

print('*******************************************')
print('Number of Bands found: ' + str(band_counter))
print('Simulation Time in Seconds: ' + str(sim_time_seconds))
print('Simulation Time in Days: ' + str(sim_time_days))
print('Simulation Time in Weeks: ' + str(sim_time_weeks))
print('Simulation Time in Months: ' + str(sim_time_months))
print('Simulation Time in Years: ' + str(sim_time_years))
print('*******************************************')

# Write the XML file for time-stepping
with open(xml_file_name_timestepping, 'w') as f:
    f.write('<!-- 2628000 s/Month  4245237 s/Week   31536000 s/Year     31536000000 s/1000Years-->\n')
    f.write('<type>FixedTimeStepping</type>\n')
    f.write('<t_initial>' + str(start_time) + '</t_initial>\n')
    f.write('<t_end>' + str(timestep_size * band_counter) + '</t_end>\n')
    f.write('<timesteps>\n')
    f.write('  <pair>\n')
    f.write('    <repeat>' + str(band_counter) + '</repeat>\n')
    f.write('    <delta_t>' + str(timestep_size) + '</delta_t>\n')
    f.write('  </pair>\n')
    f.write('</timesteps>\n')
    f.write('\n')

# Initialize timestep counter
timestep = 0
max_timestep = band_counter

# Write the XML file for recharge parameters
with open(xml_file_name_recharge, 'w') as f:
    f.write('<parameter>\n')
    f.write('  <name>RechargeOGSinput</name>\n')
    f.write('  <type>CurveScaled</type>\n')
    f.write('  <curve>mHM2OGSRechargeUnits</curve>\n')
    f.write('  <parameter>TimeDependent_Recharge</parameter>\n')
    f.write('</parameter>\n')
    f.write('\n')

    # Loop through timesteps and write recharge parameters
    for timestep in range(max_timestep):
        f.write('<parameter>\n')
        f.write('  <name>recharge_' + str(timestep) + '</name>\n')
        f.write('  <mesh>BC_Recharge_Transient</mesh>\n')
        f.write('  <type>MeshElement</type>\n')
        band_name = all_attributes.GetArrayName(timestep)
        f.write('  <field_name>' + str(band_name) + '</field_name>\n')
        f.write('</parameter>\n')

    # Write the time-dependent recharge parameter
    timestep = 0
    f.write('<parameter>\n')
    f.write('  <name>TimeDependent_Recharge</name>\n')
    f.write('  <type>TimeDependentHeterogeneousParameter</type>\n')
    f.write('  <time_series>\n')

    # Loop through timesteps and write time-series data
    for timestep in range(max_timestep):
        f.write('    <pair>\n')
        f.write('      <time>' + str(timestep_size * timestep) + '</time>\n')
        f.write('      <parameter_name>recharge_' + str(timestep) + '</parameter_name>\n')
        f.write('    </pair>\n')

    f.write('  </time_series>\n')
    f.write('</parameter>\n')

print('Writing XML files: ')
print(str(xml_file_name_recharge))
print(str(xml_file_name_timestepping))

