import sys
import os

def write_time_stepping_xml(output_file, start_time, timestep_size, number_timesteps):
    """
    Writes XML content for time-stepping parameters to a file.

    Parameters:
        output_file (str): The file path where the XML content will be written.
        start_time (int): The simulation start time.
        timestep_size (int): The size of each timestep in seconds.
        number_timesteps (int): The total number of timesteps.
    """
    with open(output_file, 'w') as f:
        f.write('<!-- 2628000 s/Month  4245237 s/Week   31536000 s/Year     31536000000 s/1000Years-->\n')
        f.write('<type>FixedTimeStepping</type>\n')
        f.write(f'<t_initial>{start_time}</t_initial>\n')
        f.write(f'<t_end>{start_time + timestep_size * number_timesteps}</t_end>\n')
        f.write('<timesteps>\n')
        f.write('  <pair>\n')
        f.write(f'    <repeat>{number_timesteps}</repeat>\n')
        f.write(f'    <delta_t>{timestep_size}</delta_t>\n')
        f.write('  </pair>\n')
        f.write('</timesteps>\n')

    print(f"Writing XML file: {output_file}")

def main():
    """
    Main function to gather input parameters and write the XML configuration file.
    """
    if len(sys.argv) < 5:
        print("Usage: python script.py <xml_output_file> <start_time> <timestep_size> <number_timesteps>")
        sys.exit(1)

    xml_output_file = sys.argv[1]
    start_time = int(sys.argv[2])
    timestep_size = int(sys.argv[3])
    number_timesteps = int(sys.argv[4])

    write_time_stepping_xml(xml_output_file, start_time, timestep_size, number_timesteps)

if __name__ == "__main__":
    main()

