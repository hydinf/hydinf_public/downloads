# by Thomas Kalbacher

print ('------ CLEANING FOLDER STRUCTURE   ------')
#import required packages
import os
import sys
import time
import shutil
import itertools
import numpy as np
import pyvista as pv
import geopandas as gpd

##########################################################################################
# The source file

folder = "OGS/ResultsSteadyState"

try:
    shutil.rmtree(folder)
except:
    os.mkdir(folder)
try:
    os.stat(folder)
except:
    os.mkdir(folder)


folder = "OGS/ResultsTransientState"

try:
    shutil.rmtree(folder)
except:
    os.mkdir(folder)
try:
    os.stat(folder)
except:
    os.mkdir(folder)


folder = "OGS/ResultsRechargeInput"

try:
    shutil.rmtree(folder)
except:
    os.mkdir(folder)
try:
    os.stat(folder)
except:
    os.mkdir(folder)


