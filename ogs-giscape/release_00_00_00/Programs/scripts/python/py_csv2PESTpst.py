import sys
import os
import csv

def create_pst_file():
    # files and parameters
    relative_input_file_path1 = str(sys.argv[1])
    relative_input_file_path2 = str(sys.argv[2])
    relative_output_file_path = str(sys.argv[3])
    wildcard_name = str(sys.argv[4])
    number_of_cores = str(sys.argv[5])
    obs_name_1 = str(sys.argv[6])
    obs_name_2 = str(sys.argv[7])

    #file_path = os.path.abspath(os.path.dirname(__file__))
    file_path = os.getcwd()
    #print(file_path)
    mat_csv_file_name = file_path + "/" + relative_input_file_path1
    obs_csv_file_name = file_path + "/" + relative_input_file_path2
    pst_file_name = file_path + "/" + relative_output_file_path
    baseflow_avg_file_name = file_path + "/DataBase/RiverLakes/Baseflow_AVG/" + str(wildcard_name) + ".csv"

    print('Creating PST File for PEST based on: ')
    print(pst_file_name)

    # Open the input CSV files for reading and the output CSV file for writing
    with open(obs_csv_file_name, 'r') as input_file, open(mat_csv_file_name, 'r') as mat_input_file, open(baseflow_avg_file_name, 'r') as baseflow_avg_input_file, open(pst_file_name, 'w', newline='') as output_file:
        # Create a CSV reader and writer objects
        reader = csv.reader(input_file)
        writer = csv.writer(output_file)  
        mat_reader = csv.reader(mat_input_file)
        bf_reader = csv.reader(baseflow_avg_input_file)
        
        # Initialize the counter
        counter = 0
        mat_counter = 0

        obs_row_count = len(list(reader)) - 1  # -1 because of header
        mat_row_count = len(list(mat_reader)) - 1
        input_file.seek(0)
        mat_input_file.seek(0)

        # Writing headers and control data for PEST
        writer.writerow(["pcf"])
        writer.writerow(["* control data"])
        writer.writerow(["restart estimation"])
        new_string = f"{mat_row_count + 1} {obs_row_count + 1} 2 0 2"  # +1 because of one baseflow observation with 2 groups
        writer.writerow([new_string])
        writer.writerow(["2 2 single point 1 0 0"])
        writer.writerow(["10.0 2.0 0.3 0.01 10"])
        writer.writerow(["5.0 5.0 1.0e-3"])
        writer.writerow([".1"])
        writer.writerow(["10 .005 4 3 .01 3"])
        writer.writerow(["1 1 1"])
        writer.writerow(["* parameter groups"])
        writer.writerow(["Kf0 relative .001 .00001 switch 1.5 best_fit"])
        writer.writerow(["St0 relative .001 .00001 switch 1.5 best_fit"])
        writer.writerow(["* parameter data"])

        # Writing material parameters
        for row in mat_reader:
            if mat_counter > 0:
                new_string = f"k{mat_counter - 1} {row[12]} factor {row[2]} {row[7]} {row[6]} Kf0 1.0 0.0 1"
                writer.writerow([new_string])
            mat_counter += 1
        writer.writerow(["s0 none factor 1.00E-10 1.00E-12 1.00E-04 St0 1.0 0.0 1"])

        # Writing observation data
        for row in reader:
            if counter == 0:
                writer.writerow(["* observation groups"])
                writer.writerow(["group_1"])
                writer.writerow(["group_2"])
                writer.writerow(["* observation data"])
            if counter > 0:
                new_string = f"{obs_name_1}{counter} {row[0]} 0.0001 group_1"
                writer.writerow([new_string])
            counter += 1

        # Writing baseflow observation
        for i, row in enumerate(bf_reader):
            if i == 1:
                new_string = f"{obs_name_2}1 {row[0]} 1.00 group_2"
                writer.writerow([new_string])
                break  # Stop reading the file once the row is found

        # Writing model command line and I/O specifications
        writer.writerow(["* model command line"])
        writer.writerow([f"./sh_mpirun_calibration_steadystate_run.sh {number_of_cores} {wildcard_name}"])
        writer.writerow(["* model input/output"])
        writer.writerow(["../MaterialData.tpl ../MaterialData.csv"])
        writer.writerow(["000_ModelRun4Calibration.tpl 000_ModelRun4Calibration.prj"])
        writer.writerow([f"../../../OGS_Results/{wildcard_name}/SteadyState/{number_of_cores}/PEST_OBS_SimResults.ins ../../../OGS_Results/{wildcard_name}/SteadyState/{number_of_cores}/PEST_OBS_SimResults.csv"])
        writer.writerow([f"../../../OGS_Results/{wildcard_name}/SteadyState/{number_of_cores}/PEST_BF_SimResults.ins ../../../OGS_Results/{wildcard_name}/SteadyState/{number_of_cores}/PEST_BF_SimResults.csv"])

def main():
    create_pst_file()

if __name__ == "__main__":
    main()
