#!/usr/bin/env python
import sys
import xml.etree.ElementTree as ET

def write_reading_rasters_xml(nc_file_name, variable_name, number_of_timestep, output_file):
    """
    Generates an XML file listing raster files for reading.

    Args:
        nc_file_name (str): The name of the NetCDF file containing raster data.
        variable_name (str): The variable of interest within the NetCDF file.
        number_of_timestep (int): The specific timestep to extract data for.
        output_file (str): The path to save the output XML file.
    """
    xml_rasters = ET.Element("rasters")
    raster = ET.SubElement(xml_rasters, "raster")
    ET.SubElement(raster, "file").text = nc_file_name
    ET.SubElement(raster, "variable").text = variable_name
    ET.SubElement(raster, "dimension").text = str(number_of_timestep)
    ET.indent(xml_rasters, space="    ", level=0)
    ET.ElementTree(xml_rasters).write(output_file)

def write_raster_parameter_xml(nc_file_name, variable_name, number_of_timestep, param_file, scaled_param_file):
    """
    Creates XML files for defining raster parameters and associated scaled parameters.

    Args:
        nc_file_name (str): The NetCDF file name.
        variable_name (str): The variable of interest within the NetCDF file.
        number_of_timestep (int): The timestep for which parameters are defined.
        param_file (str): The path to save the raster parameter XML file.
        scaled_param_file (str): The path to save the scaled parameter XML file.
    """
    raster_file_name = nc_file_name.replace(".nc", "")
    parameter = ET.Element("parameter")
    ET.SubElement(parameter, "name").text = f"{raster_file_name}_{variable_name}_{number_of_timestep}"
    ET.SubElement(parameter, "type").text = 'Raster'
    ET.indent(parameter, space="    ", level=0)
    ET.ElementTree(parameter).write(param_file)

    scaled_parameter = ET.Element("parameter")
    ET.SubElement(scaled_parameter, "name").text = 'StaticRecharge4SteadyState'
    ET.SubElement(scaled_parameter, "type").text = 'CurveScaled'
    ET.SubElement(scaled_parameter, "curve").text = 'mHM2OGSRechargeUnits'
    ET.SubElement(scaled_parameter, "parameter").text = f"{raster_file_name}_{variable_name}_{number_of_timestep}"
    ET.indent(scaled_parameter, space="    ", level=0)
    ET.ElementTree(scaled_parameter).write(scaled_param_file)

def main():
    if len(sys.argv) != 7:
        print("Usage: script.py <netcdf_file_name> <SteadyState_RasterRechargeXMLFile> <SteadyState_RasterRechargeParameters> <SteadyState_RasterRechargeParameterTimeSeries> <netcdf_variable_name> <selected_timestep>")
        sys.exit(1)

    netcdf_file_name = sys.argv[1]
    SteadyState_RasterRechargeXMLFile = sys.argv[2]
    SteadyState_RasterRechargeParameters = sys.argv[3]
    SteadyState_RasterRechargeParameterTimeSeries = sys.argv[4]
    netcdf_variable_name = sys.argv[5]
    selected_timestep = int(sys.argv[6])

    write_reading_rasters_xml(netcdf_file_name, netcdf_variable_name, selected_timestep, SteadyState_RasterRechargeXMLFile)
    write_raster_parameter_xml(netcdf_file_name, netcdf_variable_name, selected_timestep, SteadyState_RasterRechargeParameters, SteadyState_RasterRechargeParameterTimeSeries)

if __name__ == "__main__":
    main()

