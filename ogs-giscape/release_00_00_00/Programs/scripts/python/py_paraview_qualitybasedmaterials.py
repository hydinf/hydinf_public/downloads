import sys
import os
from vtk import *

def process_vtu_file(relative_input_file_path, new_mat_id, qmin, qmax):
    """
    Process a VTU file by modifying element material IDs based on element quality.

    Args:
        relative_input_file_path (str): Relative path to the input VTU file.
        new_mat_id (int): New material ID to assign to elements with quality within the specified range.
        qmin (float): Minimum acceptable quality value.
        qmax (float): Maximum acceptable quality value.

    Outputs:
        Modified VTU file with updated material IDs.
    """
    # Get the current working directory
    file_path = os.getcwd()

    # Construct full path for the input and output files
    vtu_file_name = os.path.join(file_path, relative_input_file_path)
    vtu_file_name_out = vtu_file_name

    print(vtu_file_name)

    # Initialize a VTU reader to read the input file
    reader = vtkXMLUnstructuredGridReader()
    reader.SetFileName(vtu_file_name)
    reader.Update()  # Needed because of GetScalarRange
    mesh = reader.GetOutput()
    scalar_range = mesh.GetScalarRange()
    cellIds = vtkIdList()

    # Initialize a Mesh Quality filter to compute element quality
    iq = vtkMeshQuality()
    iq.SetInputConnection(reader.GetOutputPort())

    # Access Data
    print(". . . . . . . . . .Collecting Element Information:")
    cellData = mesh.GetCellData()
    # Elements
    numberofcells = mesh.GetNumberOfCells()
    print("Processing " + str(numberofcells) + " elements")
    data = mesh.GetCellData().GetScalars("MaterialIDs")

    # Loop over each cell in the mesh
    for cell_id in range(numberofcells):
        cell = mesh.GetCell(cell_id)
        cell_type = cell.GetCellType()
        
        # Check if the cell is a Tetrahedron (cell type 10)
        if cell_type == 10:
            numberofcellpoints = cell.GetNumberOfPoints()
            mat_values = data.GetTuple1(cell_id)
            quality_values = iq.TetShape(cell)
            mat0 = int(mat_values)
            mat0_new = mat0
            qual0 = float(quality_values)
            
            # Check if the quality value is within the specified range
            if qual0 >= qmin and qual0 <= qmax:
                data.SetTuple1(cell_id, new_mat_id)
            else:
                data.SetTuple1(cell_id, mat0)

    # Write the modified mesh to a VTU file
    writer = vtkXMLUnstructuredGridWriter()
    writer.SetFileName(vtu_file_name_out)
    writer.SetDataModeToBinary()
    writer.SetInputConnection(reader.GetOutputPort())
    writer.Write()

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: python script.py <input_vtu_file> <new_material_id> <qmin> <qmax>")
    else:
        input_vtu_file = sys.argv[1]
        new_material_id = int(sys.argv[2])
        qmin = float(sys.argv[3])
        qmax = float(sys.argv[4])
        process_vtu_file(input_vtu_file, new_material_id, qmin, qmax)

