# Clipping by Thomas Kalbacher, UFZ, Leipzig, Germany

import sys
import os
import csv

def generate_material_xml(table_csv_file_name, column_nb, materialxml_file_name):
    """
    Generate material XML file based on data from a CSV table.

    Args:
        table_csv_file_name (str): Name of the CSV file containing material data.
        column_nb (int): Column number containing material properties in the CSV file.
        materialxml_file_name (str): Name of the output material XML file.

    Writes:
        XML file containing materials based on data from the CSV file.
    """
    # Get the current working directory
    file_path = os.getcwd()
    print(file_path)

    # Set paths for input and output files
    table_file = os.path.join(file_path, table_csv_file_name)
    save_as_file_name = os.path.join(file_path, materialxml_file_name)

    # Initialize a row counter
    row_counter = 0

    # Open the material XML file for writing
    with open(save_as_file_name, 'w') as f:
        # Open the CSV file for reading
        with open(table_file, newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')

            # Iterate through each row of the CSV file
            for row in reader:
                # Skip the header row
                if row_counter > 0:
                    print(row[column_nb])

                    # Writing the XML content for each medium based on the row data
                    f.write(f'<medium id="{row_counter-1}">\n')
                    f.write('  <phases>\n')
                    f.write('    <phase>\n')
                    f.write('      <type>AqueousLiquid</type>\n')
                    f.write('      <properties>\n')
                    f.write('        <property>\n')
                    f.write('          <name>viscosity</name>\n')
                    f.write('          <type>Constant</type>\n')
                    f.write('          <value>1</value>\n')
                    f.write('        </property>\n')
                    f.write('        <property>\n')
                    f.write('          <name>density</name>\n')
                    f.write('          <type>Constant</type>\n')
                    f.write('          <value>1</value>\n')
                    f.write('        </property>\n')
                    f.write('      </properties>\n')
                    f.write('    </phase>\n')
                    f.write('  </phases>\n')
                    f.write('  <properties>\n')
                    f.write('    <property>\n')
                    f.write(f'      <name>storage</name>\n')
                    f.write('      <type>Constant</type>\n')
                    f.write(f'      <value>{row[column_nb+1]}</value>\n')
                    f.write('    </property>\n')
                    f.write('    <property>\n')
                    f.write('      <name>porosity</name>\n')
                    f.write('      <type>Constant</type>\n')
                    f.write('      <value>1</value>\n')
                    f.write('    </property>\n')
                    f.write('    <property>\n')
                    f.write(f'      <name>permeability</name>\n')
                    f.write('      <type>Constant</type>\n')
                    f.write(f'      <value>{row[column_nb]}</value>\n')
                    f.write('    </property>\n')
                    f.write('    <property>\n')
                    f.write('      <name>reference_temperature</name>\n')
                    f.write('      <type>Constant</type>\n')
                    f.write('      <value>293.15</value>\n')
                    f.write('    </property>\n')
                    f.write('  </properties>\n')
                    f.write('</medium>\n')

                row_counter += 1

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("Usage: python script.py <input_csv_file> <column_number> <output_xml_file>")
    else:
        input_csv_file = sys.argv[1]
        column_number = int(sys.argv[2])
        output_xml_file = sys.argv[3]
        generate_material_xml(input_csv_file, column_number, output_xml_file)

