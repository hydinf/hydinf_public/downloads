import sys
import os
import geopandas as gpd
from shapely.geometry import box

def get_full_path(relative_path):
    """
    Constructs the full path for a file based on the current working directory and a given relative path.

    Parameters:
    relative_path (str): The relative path to the file.

    Returns:
    str: The absolute path to the file.
    """
    return os.path.join(os.getcwd(), relative_path)

def clip_shapefile(bounding_shapefile_path, shapefile_to_clip_path, output_file_path):
    """
    Clips a shapefile using the bounding box of another shapefile and saves the result as a GeoJSON.

    Parameters:
    bounding_shapefile_path (str): The file path to the bounding shapefile.
    shapefile_to_clip_path (str): The file path to the shapefile that needs to be clipped.
    output_file_path (str): The file path where the clipped shapefile will be saved as GeoJSON.
    """
    # Load the shapefiles
    shapefile_to_clip = gpd.read_file(shapefile_to_clip_path)
    bounding_shapefile = gpd.read_file(bounding_shapefile_path)

    # Calculate the bounding box and create a polygon
    bounds = bounding_shapefile.total_bounds
    bbox_polygon = box(*bounds)

    # Perform the clipping operation
    clipped_shapefile = shapefile_to_clip[shapefile_to_clip.geometry.intersects(bbox_polygon)]

    # Save the clipped shapefile as a GeoJSON
    clipped_shapefile.to_file(output_file_path, driver='GeoJSON')
    print(f"Clipped shapefile saved as {output_file_path}")

def main():
    """
    Main function to execute the shapefile clipping process based on command-line arguments.
    """
    if len(sys.argv) < 4:
        print("Usage: python script.py <bounding_shapefile> <shapefile_to_clip> <output_geojson>")
        sys.exit(1)

    bounding_shapefile_path = get_full_path(sys.argv[1])
    shapefile_to_clip_path = get_full_path(sys.argv[2])
    output_geojson_path = get_full_path(sys.argv[3])

    clip_shapefile(bounding_shapefile_path, shapefile_to_clip_path, output_geojson_path)

if __name__ == "__main__":
    main()

