wildcard_constraints:
    name="(Abens_Debug01|Abens_Debug02|Abens_Simplified|Abens_Smoothed|Ablach_6342070|Ablach_6342081|Altmühl_6342520|Altmühl_6342521|Altmühl_6342522|Altmühl_Simplified|Ammer_Simplified|DonauDE_Simplified|DonauDE_Smoothed|DonauOberlauf_6342502|DonauOberlauf_6342970|DonauOberlauf_6342980|Eschach_6342230|Glonn_6342655|GroßeLaber_6342640|Günz_Simplified|Iller_Simplified|Main_Steinbach|Mindel_6342110|Naab_1a_Simplified|Naab_1ab_Simplified|Naab_2a_Simplified|Naab_All_Simplified|Regen_Simplified|Riss_6342100|Rot_6342105|Rott_Simplified|Strogen_6342660|Sulz_6342525|Vils_6342670|Vils_Simplified|Wörnitz_6342130|Zusam_6342120|Zusam_Simplified)"

#TEMPLATES

# Step 1: Create Model and Run Simulation for Calibration
#      "OGS_Setups/Günz_Simplified/PEST_PST_STEADY_STATE/pest_steadystate_calibration.sen"

# with *.pst, all required files will be prepared, and a pestchek will be executed but not the calibration run
#      "OGS_Setups/Günz_Simplified/PEST_PST_STEADY_STATE/pest_steadystate_calibration.pst"


# Step 2: Reorganize Parmeter-Setup after Calibration and Update DataBase manually (Material Properties)

# Step 3: Create Model and Run Simulation for Baseflow Analysis
#      "OGS_Results/Günz_Simplified/ResultsTransientState/MOD_01_OUT_Rivers.pvd",

# Step 4: Create Recharge Input for Visualisation and Analysis
#      "OGS_Results/Günz_Simplified/ResultsRechargeInput/BaseflowAnalysisMode_02.txt",

##############################################################################
######### INCLUDES ###########################################################
##############################################################################
include: "snakefile_global_settings"
include: "snakefile_bc_setup_river"
include: "snakefile_bc_setup_recharge"
include: "snakefile_ogs_setup"
include: "snakefile_create_meshes"
include: "snakefile_calibration_setup"
##############################################################################
######### TARGET DEFINITION '#################################################
##############################################################################
rule TargetFiles:
     input:
        RunSimulation + "Abens_Debug01" + TransientSubPath + '02/' + TransientResultFile,
#        RunSimulation + "Main_Steinbach" + TransientSubPath + '16/' + TransientResultFile,
#        RunSimulation + "Sulz_6342525" + TransientSubPath + '06/' + TransientResultFile,
#        RunSimulation + "Günz_Simplified" + TransientSubPath + '06/' + TransientResultFile,
#        RunSimulation + "Iller_Simplified" + TransientSubPath + '06/' + TransientResultFile,
#        RunSimulation + "Riss_6342100" + TransientSubPath + '06/' + TransientResultFile,
#        RunCalibration + "Strogen_6342660" + SteadyStateSubPath + '06/' + PEST,


#TEMPLATES

# Step 1: Create Model and Run Simulation for Calibration 
#      RunCalibration + "Strogen_6342660" + SteadyStateSubPath + '06/' + PEST, 

# with *.pst, all required files will be prepared, and a pestchek will be executed but not the calibration run 
#      RunCalibration + "Strogen_6342660" + SteadyStateSubPath + '06/' + PESTCHECK,

# Step 2: Reorganize Parmeter-Setup after Calibration and Update DataBase manually (Material Properties)

# Step 3: Create Model and Run Simulation for Baseflow Analysis
#       RunSimulation + "Strogen_6342660" + TransientSubPath + '06/' + TransientResultFile,

##############################################################################



##############################################################################
######### WORKSPACE ##########################################################
##############################################################################



rule Link_SteadyState_OGS_ProjectFile_ForParallelSimulation:
    input:
        "OGS_Setups/{name}/SteadyState/000_ModelRun.prj",
    output:
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/000_ModelRun.prj",
    shell:
        """
        cd OGS_Setups/{wildcards.name}/SteadyState/{wildcards.steady_state_mpi_processes}
        ln -rsf ../000_ModelRun.prj 000_ModelRun.prj
        cd ../../../..
        """

rule Link_SteadyState_OGS_ProjectFile_ForCalibrationRuns:
    input:
        "OGS_Setups/{name}/SteadyState/000_ModelRun4Calibration.prj",
    output:
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/000_ModelRun4Calibration.prj",
    shell:
        """
        cd OGS_Setups/{wildcards.name}/SteadyState/{wildcards.steady_state_mpi_processes}
        ln -rsf ../000_ModelRun4Calibration.prj 000_ModelRun4Calibration.prj
        cd ../../../..
        """


rule Prepare_SteadyState_AdditionalProjectFiles_ForParallelSimulation:
    input:
        "OGS_Setups/{name}/SteadyState/000_Media.xml",
        "OGS_Setups/{name}/SteadyState/000_TimeStepping.xml",
        "OGS_Setups/{name}/SteadyState/000_RasterRechargeXMLFile.xml",
        "OGS_Setups/{name}/SteadyState/000_RasterRechargeParameters.xml",
        "OGS_Setups/{name}/SteadyState/000_RasterRechargeParameterTimeSeries.xml",
    output:
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/000_Media.xml",
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/000_TimeStepping.xml",
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/000_RasterRechargeXMLFile.xml",
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/000_RasterRechargeParameters.xml",
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/000_RasterRechargeParameterTimeSeries.xml",
    shell:
        """
        cd OGS_Setups/{wildcards.name}/SteadyState/{wildcards.steady_state_mpi_processes}
        ln -rsf ../000_Media.xml 000_Media.xml
        ln -rsf ../000_TimeStepping.xml 000_TimeStepping.xml
        ln -rsf ../000_RasterRechargeXMLFile.xml 000_RasterRechargeXMLFile.xml
        ln -rsf ../000_RasterRechargeParameters.xml 000_RasterRechargeParameters.xml
        ln -rsf ../000_RasterRechargeParameterTimeSeries.xml 000_RasterRechargeParameterTimeSeries.xml
        cd ../../../..
        """

rule Prepare_Transient_ProjectFiles_ForParallelSimulation:
    input:
        "OGS_Setups/{name}/Transient/000_Media.xml",
        "OGS_Setups/{name}/Transient/001_ModelRun.prj",
        "OGS_Setups/{name}/Transient/001_TimeStepping.xml",
        "OGS_Setups/{name}/Transient/001_RasterRechargeXMLFile.xml",
        "OGS_Setups/{name}/Transient/001_RasterRechargeParameters.xml",
        "OGS_Setups/{name}/Transient/001_RasterRechargeParameterTimeSeries.xml",
    output:
        "OGS_Setups/{name}/Transient/{mpi_processes}/000_Media.xml",
        "OGS_Setups/{name}/Transient/{mpi_processes}/001_ModelRun.prj",
        "OGS_Setups/{name}/Transient/{mpi_processes}/001_TimeStepping.xml",
        "OGS_Setups/{name}/Transient/{mpi_processes}/001_RasterRechargeXMLFile.xml",
        "OGS_Setups/{name}/Transient/{mpi_processes}/001_RasterRechargeParameters.xml",
        "OGS_Setups/{name}/Transient/{mpi_processes}/001_RasterRechargeParameterTimeSeries.xml",
    shell:
        """
        cd OGS_Setups/{wildcards.name}/Transient/{wildcards.mpi_processes}
        ln -rsf ../000_Media.xml 000_Media.xml
        ln -rsf ../001_ModelRun.prj 001_ModelRun.prj
        ln -rsf ../001_TimeStepping.xml 001_TimeStepping.xml
        ln -rsf ../001_RasterRechargeXMLFile.xml 001_RasterRechargeXMLFile.xml
        ln -rsf ../001_RasterRechargeParameters.xml 001_RasterRechargeParameters.xml
        ln -rsf ../001_RasterRechargeParameterTimeSeries.xml 001_RasterRechargeParameterTimeSeries.xml
        cd ../../../..
        """

rule Prepare_SteadyState_Meshes:
    input:
        "OGS_Setups/{name}/OUT_Rivers.vtu",
        "OGS_Setups/{name}/OUT_Recharge.vtu",
        "OGS_Setups/{name}/Subsurface3D.vtu",
        "OGS_Setups/{name}/Surface3D.vtu",
        "OGS_Setups/{name}/BC_RiverLakes_Deutschland.vtu",
    output:
        "OGS_Setups/{name}/SteadyState/OUT_Rivers.vtu",
        "OGS_Setups/{name}/SteadyState/OUT_Recharge.vtu",
        "OGS_Setups/{name}/SteadyState/Subsurface3D.vtu",
        "OGS_Setups/{name}/SteadyState/Surface3D.vtu",
        "OGS_Setups/{name}/SteadyState/BC_RiverLakes_Deutschland.vtu",
    shell:
        """
        cd OGS_Setups/{wildcards.name}/SteadyState
        ln -rsf ../OUT_Rivers.vtu
        ln -rsf ../OUT_Recharge.vtu
        ln -rsf ../Subsurface3D.vtu
        ln -rsf ../Surface3D.vtu
        ln -rsf ../BC_RiverLakes_Deutschland.vtu
        cd ../../..
        """

rule Prepare_Transient_Meshes:
    input:
        "OGS_Setups/{name}/OUT_Rivers.vtu",
        "OGS_Setups/{name}/OUT_Recharge.vtu",
        "OGS_Setups/{name}/Surface3D.vtu",
        "OGS_Setups/{name}/BC_RiverLakes_Deutschland.vtu",
    output:
        "OGS_Setups/{name}/Transient/OUT_Rivers.vtu",
        "OGS_Setups/{name}/Transient/OUT_Recharge.vtu",
        "OGS_Setups/{name}/Transient/Surface3D.vtu",
        "OGS_Setups/{name}/Transient/BC_RiverLakes_Deutschland.vtu",
    shell:
        """
        cd OGS_Setups/{wildcards.name}/Transient
        ln -rsf ../OUT_Rivers.vtu
        ln -rsf ../OUT_Recharge.vtu
        ln -rsf ../Surface3D.vtu
        ln -rsf ../BC_RiverLakes_Deutschland.vtu
        cd ../../..
        """

##############################################################################
######### Simulation Calls  (Target Rules) ###################################
##############################################################################

rule OGS_SteadyState_ParallelSimulation:
    input:
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/000_Media.xml",
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/000_ModelRun.prj",
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/000_TimeStepping.xml",
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/000_RasterRechargeXMLFile.xml",
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/000_RasterRechargeParameters.xml",
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/000_RasterRechargeParameterTimeSeries.xml",
        "OGS_Setups/{name}/SteadyState/OUT_Rivers.vtu",
        "OGS_Setups/{name}/SteadyState/OUT_Recharge.vtu",
        "OGS_Setups/{name}/SteadyState/Subsurface3D.vtu",
        "OGS_Setups/{name}/SteadyState/Surface3D.vtu",
        "OGS_Setups/{name}/SteadyState/BC_RiverLakes_Deutschland.vtu",
        "OGS_Setups/{name}/SteadyState/BC_Recharge_Steady.nc",
        "OGS_Setups/{name}/SteadyState/sh_mpirun_steadystate_only.sh",
    output:
        "OGS_Results/{name}/SteadyState/{steady_state_mpi_processes}/MOD_01_Subsurface3D.pvd",
    benchmark:
        "DataSets/{name}/SteadyState/{steady_state_mpi_processes}/OGS_SteadyState_ParallelSimulation.bench"
    shell:
        """
        python3 Programs/scripts/python/py_generate_snakemake_rule_perfomance_csv.py DataSets/{wildcards.name}
        cd OGS_Setups/{wildcards.name}/SteadyState
        ./sh_mpirun_steadystate_only.sh {wildcards.steady_state_mpi_processes} {wildcards.name}
        cd ../../..
        """

rule OGS_Transient_ParallelSimulation:
    input:
        "OGS_Setups/{name}/Transient/{mpi_processes}/000_Media.xml",
        "OGS_Setups/{name}/Transient/{mpi_processes}/001_ModelRun.prj",
        "OGS_Setups/{name}/Transient/{mpi_processes}/001_TimeStepping.xml",
        "OGS_Setups/{name}/Transient/{mpi_processes}/001_RasterRechargeXMLFile.xml",
        "OGS_Setups/{name}/Transient/{mpi_processes}/001_RasterRechargeParameters.xml",
        "OGS_Setups/{name}/Transient/{mpi_processes}/001_RasterRechargeParameterTimeSeries.xml",
        "OGS_Setups/{name}/Transient/OUT_Rivers.vtu",
        "OGS_Setups/{name}/Transient/OUT_Recharge.vtu",
        "OGS_Setups/{name}/Transient/Surface3D.vtu",
        "OGS_Setups/{name}/Transient/BC_Recharge_Transient.nc",
        "OGS_Setups/{name}/Transient/BC_RiverLakes_Deutschland.vtu",
        "OGS_Setups/{name}/Transient/sh_mpirun_transient_only.sh",
        "OGS_Results/{name}/SteadyState/{mpi_processes}/MOD_01_Subsurface3D.pvd",
    output:
        "OGS_Results/{name}/Transient/{mpi_processes}/MOD_01_OUT_Rivers.pvd",
    benchmark:
        "DataSets/{name}/Transient/{mpi_processes}/OGS_Transient_ParallelSimulation.bench"
    shell:
        """
        cd OGS_Setups/{wildcards.name}/Transient
        ./sh_mpirun_transient_only.sh {wildcards.mpi_processes} {wildcards.name}
        cd ../../..
        """

rule PEST_Calibration_OGS_SteadyState_ParallelSimulation:
    input:
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/000_Media.xml",
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/000_ModelRun4Calibration.prj",
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/000_TimeStepping.xml",
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/000_RasterRechargeXMLFile.xml",
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/000_RasterRechargeParameters.xml",
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/000_RasterRechargeParameterTimeSeries.xml",
        "OGS_Setups/{name}/SteadyState/MonitoringWells.vtu",
        "OGS_Setups/{name}/SteadyState/Subsurface3D.vtu",
        "OGS_Setups/{name}/SteadyState/Surface3D.vtu",
        "OGS_Setups/{name}/SteadyState/BC_RiverLakes_Deutschland.vtu",
        "OGS_Setups/{name}/SteadyState/BC_Recharge_Steady.nc",
        "OGS_Setups/{name}/SteadyState/sh_mpirun_calibration_steadystate_setup.sh",
    output:
        "OGS_Results/{name}/SteadyState/{steady_state_mpi_processes}/MOD_01_MonitoringWells_ts_10_t_315360000000_000000.pvtu",
        "OGS_Results/{name}/SteadyState/{steady_state_mpi_processes}/MOD_01_BC_RiverLakes_Deutschland_ts_10_t_315360000000_000000.pvtu",
    benchmark:
        "DataSets/{name}/SteadyState/{steady_state_mpi_processes}/OGS_SteadyState_ParallelSimulation.bench"
    shell:
        """
        cd OGS_Setups/{wildcards.name}/SteadyState
        ./sh_mpirun_calibration_steadystate_setup.sh {wildcards.steady_state_mpi_processes} {wildcards.name}
        cd ../../..
        """



rule PEST_Calibration_Set_Check_1st_Run:
    input:
        "OGS_Setups/{name}/MaterialData.csv",
        "OGS_Setups/{name}/MaterialData.tpl",
        "DataSets/{name}/Wells/MonitoringWells_{name}_Observed_Average_Head.csv",
        "OGS_Results/{name}/SteadyState/{steady_state_mpi_processes}/PEST_OBS_SimResults.csv",
        "OGS_Results/{name}/SteadyState/{steady_state_mpi_processes}/PEST_OBS_SimResults.ins",
        "OGS_Results/{name}/SteadyState/{steady_state_mpi_processes}/PEST_BF_SimResults.csv",
        "OGS_Results/{name}/SteadyState/{steady_state_mpi_processes}/PEST_BF_SimResults.ins",
    output:
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/PEST_PST_STEADY_STATE/pest_steadystate_calibration.pst",
    benchmark:
        "DataSets/{name}/SteadyState/{steady_state_mpi_processes}/PEST_Calibration_Set_Check_1st_Run.bench"
    shell:
        """
        rm -rf OGS_Setups/{wildcards.name}/SteadyState/{wildcards.steady_state_mpi_processes}/PEST_PST_STEADY_STATE
        mkdir OGS_Setups/{wildcards.name}/SteadyState/{wildcards.steady_state_mpi_processes}/PEST_PST_STEADY_STATE
        python3 Programs/scripts/python/py_csv2PESTpst.py {input[0]} {input[2]} {output[0]} {wildcards.name} {wildcards.steady_state_mpi_processes} OBS_ BF_
        cd OGS_Setups/{wildcards.name}/SteadyState/
        ../../../Programs/bin/pestchek {wildcards.steady_state_mpi_processes}/PEST_PST_STEADY_STATE/pest_steadystate_calibration.pst
        cd ../../..
        """



rule PEST_Calibration_and_Sensitivity_Analysis:
    input:
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/PEST_PST_STEADY_STATE/pest_steadystate_calibration.pst",
    output: 
        "OGS_Setups/{name}/SteadyState/{steady_state_mpi_processes}/PEST_PST_STEADY_STATE/pest_steadystate_calibration.sen"    
    benchmark:
        "DataSets/{name}/SteadyState/{steady_state_mpi_processes}/PEST_Calibration_and_Sensitivity_Analysis.bench"
    shell:  
        """
        python3 Programs/scripts/python/py_generate_snakemake_rule_perfomance_csv.py DataSets/{wildcards.name}
        cd OGS_Setups/{wildcards.name}/SteadyState
        ./sh_mpirun_calibration_steadystate_setup.sh {wildcards.steady_state_mpi_processes} {wildcards.name}
        ./sh_mpirun_calibration_steadystate_run.sh {wildcards.steady_state_mpi_processes} {wildcards.name}
		../../../Programs/bin/pestpp-glm {wildcards.steady_state_mpi_processes}/PEST_PST_STEADY_STATE/pest_steadystate_calibration.pst
        cd ../../..
        """
