import sys
import os
from vtk import *

def process_vtu_file(relative_input_file_path, target_mat_id=22, nogo_mat_id=0):
    """
    Process a VTU file to update material IDs.
    
    Parameters:
        relative_input_file_path (str): Relative path to the input VTU file.
        target_mat_id (int): Target material ID to be replaced.
        nogo_mat_id (int): Material ID to be excluded from replacement.
    """
    # Get the current working directory
    file_path = os.getcwd()

    # Construct full path for the input and output files
    vtu_file_name = file_path + relative_input_file_path
    vtu_file_name_out = vtu_file_name

    print(vtu_file_name)

    # Initialize a VTU reader to read the input file
    reader = vtkXMLUnstructuredGridReader() # VTU Reader
    reader.SetFileName(vtu_file_name)
    reader.Update() # Needed because of GetScalarRange
    mesh = reader.GetOutput()
    scalar_range = mesh.GetScalarRange()
    cellIds = vtkIdList()

    # Access Data
    print(". . . . . . . . . .Collecting Element Information:")
    cellData = mesh.GetCellData()

    # Elements
    numberofcells = mesh.GetNumberOfCells()
    print("Processing " + str(numberofcells) + " elements")
    data = mesh.GetCellData().GetScalars("MaterialIDs")

    # Initialize a variable to check if there are still target hits
    targethit = 1

    # Loop until there are no more target hits
    while targethit == 1:
        targethit = 0

        # Loop over each cell in the mesh
        for cell_id in range(numberofcells):
            cell = mesh.GetCell(cell_id)
            cell_type = cell.GetCellType()
            numberofcellpoints = cell.GetNumberOfPoints()
            mat_values = data.GetTuple1(cell_id)
            mat0 = int(mat_values)
            mat0_new = mat0

            # Check if the current cell's material ID matches the target
            if mat0 == target_mat_id:
                targethit = 1

                # Loop over each point in the current cell
                for point_iterator in range(numberofcellpoints):
                    point_id = cell.GetPointId(point_iterator)
                    mesh.GetPointCells(point_id, cellIds)

                    # Loop over neighboring cells of the current point
                    for neighbor_iterator in range(cellIds.GetNumberOfIds()):
                        nbh_id = cellIds.GetId(neighbor_iterator)
                        nbh_mat_value = data.GetTuple1(nbh_id)
                        nbh_mat0 = int(nbh_mat_value)

                        # Check if the neighbor's material ID is different from the current cell's and not a no-go material
                        if mat0 != nbh_mat0 and nbh_mat0 != nogo_mat_id:
                            mat0_new = nbh_mat0

            # Set the new material ID for the current cell
            data.SetTuple1(cell_id, mat0_new)

    # Write the modified mesh to a VTU file
    writer = vtkXMLUnstructuredGridWriter()
    writer.SetFileName(vtu_file_name_out)
    writer.SetDataModeToBinary()
    writer.SetInputConnection(reader.GetOutputPort())
    writer.Write()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python script.py <relative_input_file_path>")
    else:
        relative_input_file_path = sys.argv[1]
        process_vtu_file(relative_input_file_path)

