#Clipping by Thomas Kalbacher, UFZ, Leipzig, Germany
##########################################################################################
import sys
import os
from datetime import datetime
from vtk import *
from paraview.simple import *
import numpy as np
##########################################################################################
file_path = os.getcwd()
#-----------------------------------------------------------------------------------------
vtu_input_filename = str(sys.argv[1])
csv_output_filename = str(sys.argv[2])
print('. . . Converting VTU to CSV - Head and Coordinates only')

DomainVTU = XMLPartitionedUnstructuredGridReader(registrationName='Domain', FileName=[vtu_input_filename])
#DomainVTU = XMLUnstructuredGridReader(registrationName='Domain', FileName=[vtu_input_filename])

# Properties modified on mOD_01_BC_RiverLakes_Deutschland_ts_10_t_315360000000_000000pvtu
DomainVTU.CellArrayStatus = []
DomainVTU.PointArrayStatus = ['VolumetricFlowRate', 'head']
DomainVTU.TimeArray = 'None'
UpdatePipeline(time=0.0, proxy=DomainVTU)


# create a new 'Descriptive Statistics'
descriptiveStatistics1 = DescriptiveStatistics(registrationName='DescriptiveStatistics1', Input=DomainVTU, ModelInput=None)
descriptiveStatistics1.VariablesofInterest = ['VolumetricFlowRate']
UpdatePipeline(time=0.0, proxy=descriptiveStatistics1)

# create a new 'PassArrays'
passArrays1 = PassArrays(registrationName='PassArrays1', Input=descriptiveStatistics1)
passArrays1.RowDataArrays = ['Sum']
UpdatePipeline(time=0.0, proxy=passArrays1)


print ('Saving File: ' + str(csv_output_filename))
# save data
SaveData(csv_output_filename, proxy=passArrays1, RowDataArrays=['Sum'], Precision=10, UseScientificNotation=1, FieldAssociation='Row Data', AddMetaData=0)

