#!/usr/bin/env bash

script_dir="$(dirname "$0")"

source ${script_dir}/compareWithDiff.sh
source ${script_dir}/compareWithVtkDiff.sh

differences=0 # assume all is okay

reference_model=Abens_Debug01

# compare files in DataSets
ref_path=tests/workflow_reference_files/DataSets/${reference_model}
result_path=DataSets/${reference_model}

declare -a general_data_set_files=(
    "./RiverLakes/Rivers.shp"
    "./RiverLakes/Rivers.shx"
    # "./RiverLakes/Rivers.dbf"
    # "./RiverLakes/Rivers.cpg"
    # "./RiverLakes/Rivers.prj"
    "./RiverLakes/RiversPointsFromDataBase.geojson"
    # "./RiverLakes/remove_overlapping_riverpoints_protocol.txt"
    # "./RiverLakes/correct_river_steepness_protocol.txt"
    "./Wells/Wells.shp"
    "./Wells/Wells.shx"
    # "./Wells/Wells.dbf"
    # "./Wells/Wells.cpg"
    # "./Wells/Wells.prj"
    # "./Hydrogeology/Domain2D.vtk"
    # "./Hydrogeology/Domain2D_CheckMeshProtocol.txt"
    # "./Hydrogeology/createLayeredMeshFromRastersProtocol.txt"
    # "./Hydrogeology/py_paraview_replaceMATbyNeighbor_protocol.txt"
    # "./Hydrogeology/py_paraview_qualitybasedmaterials_protocol.txt"
    # "./Hydrogeology/nodereordering_protocol.txt"
)

declare -a vtu_data_set_files=(
    "./Hydrogeology/Subsurface3D.vtu"
    "./Hydrogeology/Domain2D.vtu"
    "./RiverLakes/RiversPointsSteepnessCorrection.vtu"
    "./RiverLakes/RiversPointsFromDataBase.vtu"
    "./RiverLakes/RiversPoints.vtu"
    "./Hydrogeology/Surface3D.vtu"
)

diffFiles $ref_path $result_path general_data_set_files
diffVTKFiles $ref_path $result_path vtu_data_set_files

# compare files in OGS_Setups
ref_path=tests/workflow_reference_files/OGS_Setups/${reference_model}
result_path=OGS_Setups/${reference_model}

declare -a base_files=("MaterialData.csv")
declare -a steady_state_files=(
    # "./SteadyState/BC_Recharge_Steady_Clipped.tif"
    "./SteadyState/BC_Recharge_Steady_Average.csv"
    "./SteadyState/000_RasterRechargeXMLFile.xml"
    "./SteadyState/000_RasterRechargeParameters.xml"
    "./SteadyState/000_RasterRechargeParameterTimeSeries.xml"
    "./SteadyState/000_ModelRun.prj"
    "./SteadyState/000_Media.xml"
    "./SteadyState/000_TimeStepping.xml"
)
declare -a transient_files=(
    # "./Transient/BC_Recharge_Transient_Clipped.tif"
    "./Transient/BC_Recharge_Transient_Average.csv"
    "./Transient/001_RasterRechargeXMLFile.xml"
    "./Transient/001_RasterRechargeParameterTimeSeries.xml"
    "./Transient/001_RasterRechargeParameters.xml"
    "./Transient/001_TimeStepping.xml"
    "./Transient/001_ModelRun.prj"
    "./Transient/000_Media.xml"
)
declare -a vtu_files=(
    "./Subsurface3D.vtu"
    "./Surface3D.vtu"
    "./BC_RiverLakes_Deutschland.vtu"
    "./OUT_Recharge.vtu"
    "./OUT_Rivers.vtu"
)
declare -a transient_vtu_files=(
    "./Transient/Subsurface3D_IC.vtu"
)

diffFiles ${ref_path} ${result_path} base_files
diffFiles $ref_path $result_path steady_state_files
if [[ $? == '1' ]];
then
    echo 'steady state model files are identical'
else
    echo 'steady state model files have differences'
    differences=1
fi

diffFiles $ref_path $result_path transient_files
if [[ $? == '1' ]];
then
    echo 'transient model files are identical'
else
    echo 'transient model files have differences'
    differences=1
fi

diffVTKFiles $ref_path $result_path vtu_files
if [[ $? == '1' ]];
then
    echo 'vtu files are identical'
else
    echo 'vtu files have differences'
    differences=1
fi

diffVTKFiles $ref_path $result_path transient_vtu_files
if [[ $? == '1' ]];
then
    echo 'transient vtu files are identical'
else
    echo 'transient vtu files have differences'
    differences=1
fi

exit ${differences}
