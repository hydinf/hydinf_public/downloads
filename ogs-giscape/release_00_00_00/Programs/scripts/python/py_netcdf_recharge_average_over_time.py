import os
import sys
import xarray as xr
import geopandas as gpd
import pandas as pd

def get_full_path(relative_path):
    """
    Constructs the full path for a file based on the current working directory and a given relative path.

    Parameters:
    relative_path (str): The relative path to the file.

    Returns:
    str: The absolute path to the file.
    """
    return os.path.join(os.getcwd(), relative_path)

def read_and_process_shapefile(shapefile_path, buffer_distance=2000):
    """
    Reads a shapefile and calculates an expanded bounding box.

    Parameters:
    shapefile_path (str): The path to the shapefile.
    buffer_distance (int): The distance to expand the bounding box on each side.

    Returns:
    tuple: The coordinates of the expanded bounding box (minx, miny, maxx, maxy).
    """
    gdf = gpd.read_file(shapefile_path)
    bounds = gdf.total_bounds
    minx, miny, maxx, maxy = bounds
    return (minx - buffer_distance, miny - buffer_distance, maxx + buffer_distance, maxy + buffer_distance)

def extract_and_average_recharge(netcdf_file_path, bounds):
    """
    Extracts recharge data within specified bounds and computes the average over time.

    Parameters:
    netcdf_file_path (str): The path to the NETCDF file.
    bounds (tuple): The bounding coordinates (minx, miny, maxx, maxy).

    Returns:
    pd.DataFrame: DataFrame with the average recharge values or None if the 'recharge' variable is not found.
    """
    ds = xr.open_dataset(netcdf_file_path)
    if 'recharge' in ds.variables:
        recharge = ds['recharge'].sel(x=slice(*bounds[::2]), y=slice(*bounds[1::2]))
        avg_recharge = recharge.mean(dim=['x', 'y']).to_dataframe(name='AverageRecharge').reset_index()
        return avg_recharge
    else:
        print("The variable 'recharge' does not exist in the dataset.")
        return None

def save_dataframe_to_csv(dataframe, csv_file_path):
    """
    Saves the DataFrame to a CSV file.

    Parameters:
    dataframe (pd.DataFrame): The DataFrame to save.
    csv_file_path (str): The path to the output CSV file.
    """
    if dataframe is not None:
        dataframe.to_csv(csv_file_path, index=False)
        print(f'File has been successfully saved as {csv_file_path}.')
    else:
        print("No data to save.")

def main():
    """
    Main function to execute the workflow of reading shapefiles, processing NETCDF data, and saving to a CSV file.
    """
    if len(sys.argv) < 4:
        print("Usage: python script.py <shapefile> <netcdf_file> <output_csv>")
        sys.exit(1)
    
    shape_file_path = get_full_path(sys.argv[1])
    netcdf_file_path = get_full_path(sys.argv[2])
    csv_file_path = get_full_path(sys.argv[3])

    bounds = read_and_process_shapefile(shape_file_path)
    print("Bounding box: ", bounds)
    print("delta x = ", bounds[2] - bounds[0])
    print("delta y = ", bounds[3] - bounds[1])

    df = extract_and_average_recharge(netcdf_file_path, bounds)
    save_dataframe_to_csv(df, csv_file_path)

if __name__ == "__main__":
    main()

