import sys
import os
import csv

def create_ins_file():
    # files and parameters
    relative_input_file_path = str(sys.argv[1])
    relative_output_file_path = str(sys.argv[2])
    OBS_NAME = str(sys.argv[3])
    #file_path = os.path.abspath(os.path.dirname(__file__))
    file_path = os.getcwd()
    #print(file_path)
    csv_file_name = file_path + "/" + relative_input_file_path
    ins_file_name_out = file_path + "/" + relative_output_file_path

    print('Creating INS File for PEST based on: ')
    print(csv_file_name)

    # Open the input CSV file for reading and the output CSV file for writing
    with open(csv_file_name, 'r') as input_file, open(ins_file_name_out, 'w', newline='') as output_file:
        
        # Create a CSV reader and writer objects
        reader = csv.reader(input_file)
        writer = csv.writer(output_file)
        
        # Initialize the counter
        counter = 0
        # Iterate over each row in the input CSV file
        for row in reader:
            if counter == 0:
                row = ["pif @"]
                writer.writerow(row)
            if counter == 1:
                new_string = "l02 [" + str(OBS_NAME) + str(counter) + "]1:17" 
                row = [new_string]
                writer.writerow(row)
            if counter > 1:
                new_string = "l01 [" + str(OBS_NAME) + str(counter) + "]1:17" 
                row = [new_string]
                writer.writerow(row)
            counter += 1

def main():
    create_ins_file()

if __name__ == "__main__":
    main()
