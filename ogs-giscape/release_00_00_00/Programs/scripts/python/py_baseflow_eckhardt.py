#!/usr/bin/env python
import numpy as np
from scipy.optimize import minimize
import warnings

# Suppress warnings for cleaner output
warnings.filterwarnings('ignore')

#=============================================================================
# Functions
#=============================================================================

def filter_Eck(alpha, perc, data_D):
    """
    Applies the Eckhardt filter to discharge data to estimate baseflow.

    Parameters
    ----------
    alpha : float
        Eckhardt parameter alpha, controls separation of quickflow and baseflow.
    perc : float
        Percentile used to determine BFImax, affects baseflow separation.
    data_D : numpy.ndarray
        Array of discharge data to be filtered.

    Returns
    -------
    numpy.ndarray
        The estimated baseflow series.
    """
    # Smooth discharge data with a 7-day moving average
    data = Q_7(data_D)

    # Determine BFImax using the specified percentile of the smoothed data
    Q7_perc = np.percentile(data, perc)
    BFImax = Q7_perc / np.mean(data_D)

    # Initialize baseflow series and apply Eckhardt filter equation
    res = np.zeros(len(data))
    res[0] = ((1 - alpha) * BFImax * data_D[0]) / (1 - alpha * BFImax)
    for i in range(1, len(data)):
        res[i] = ((1 - BFImax) * alpha * res[i-1] + (1 - alpha) * BFImax * data_D[i]) / (1 - alpha * BFImax)

    return res

def alpha_opt(Qobs, perc, method):
    """
    Optimizes the Eckhardt filter parameter alpha to minimize the difference between
    observed and filtered discharge during low-flow periods.

    Parameters
    ----------
    Qobs : numpy.ndarray
        Observed discharge data.
    perc : float
        Percentile for determining BFImax in the Eckhardt filter.
    method : str
        Optimization method to use.

    Returns
    -------
    float
        Optimized value of alpha.
    """
    # Smooth the observed discharge data
    Qobs7 = Q_7(Qobs)

    # Determine low-flow points
    Qmin = [min(Qobs7[i:i+365]) for i in range(0, len(Qobs), 365)]
    Qmin_index = np.where(Qobs7 <= np.percentile(Qmin, 10))[0]

    # Objective function for optimization: minimize the squared difference at low-flow points
    def func_mean_prop(alpha):
        filtered = filter_Eck(alpha, perc, Qobs7)
        return np.mean((filtered[Qmin_index] / Qobs7[Qmin_index] - 1) ** 2)

    # Perform optimization to find the best alpha
    alpha_0 = 0.5
    result = minimize(func_mean_prop, alpha_0, bounds=[(0., 1.)], method=method)

    return result.x[0]

def BF_Eck(data, perc=20, method='nelder-mead'):
    """
    Estimates baseflow from discharge data using the Eckhardt filter.

    Parameters
    ----------
    data : numpy.ndarray
        Discharge data.
    perc : float, optional
        Percentile for BFImax calculation, by default 20.
    method : str, optional
        Optimization method for alpha, by default 'nelder-mead'.

    Returns
    -------
    numpy.ndarray
        Estimated baseflow series.
    """
    # Optimize alpha and apply the Eckhardt filter
    alpha = alpha_opt(data, perc, method)
    QB = filter_Eck(alpha, perc, data)

    return QB

def Q_7(data):
    """
    Computes a 7-day moving average of the discharge data.

    Parameters
    ----------
    data : numpy.ndarray
        Discharge data.

    Returns
    -------
    numpy.ndarray
        7-day averaged discharge data.
    """
    Qobs7 = np.zeros(len(data))
    for i in range(len(data)):
        start_idx = max(i-3, 0)
        end_idx = min(i+4, len(data))
        Qobs7[i] = np.mean(data[start_idx:end_idx])

    return Qobs7

