RED='\033[0;31m'
BLUE='\033[0;34m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m' # No Color
set -e


#####################################################################################
# SETUP 
achive_name="Archive_00"
geo_input_name="GMSH/$1.geo"
mesh_input_name="/GMSH/$1.vtk"
station_number=$(echo $1 | sed 's/:*mask_//')
python3 WF_Scripts/py_clean_folder_structure.py
rm -rf "OGS/$achive_name/$1"
mkdir "OGS/$achive_name/$1"
#####################################################################################



#####################################################################################
time0=$(date +"%T")
echo -e "${YELLOW}#############################################################${NC}"
echo "START OF WORKFLOW"
echo -e "${YELLOW}#############################################################${NC}"
#####################################################################################
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
echo "- taking 2D Domain $1 from GEO or VTK file"
echo -e "${YELLOW}#############################################################${NC}"
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
#./GMSH/gmsh -2 $geo_input_name -algo meshadapt -format msh2 -epslc1d 1e-3 -format vtk -clmax 300 -clmin 50 -smooth 10
python3 WF_Scripts/py_vtk2vtu_unstructuredgrid.py $mesh_input_name /OGS/Domain2D.vtu

#singularity exec ./OGS/ogs.sif checkMesh ./OGS/Domain2D.vtu -v
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
echo "- xy-clipping of the 2D Domain"
echo "- Box is defined by: xmin, xmax, ymin, ymax  "
echo ${ClipCoordinates}

#pvpython WF_Scripts/py_pv_clipvtu_xmin_xmax_ymin_ymax.py 400000.0 860000.00 5000000.00 6000000.00 /OGS/Domain2D.vtu
echo "Done"
echo " "
time1=$(date +"%T")
#####################################################################################
echo -e "${YELLOW}#############################################################${NC}"
echo "Start: create Multi-Layer Model (ASC-Layers) "
echo -e "${YELLOW}#############################################################${NC}"
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
echo "Start: createLayeredMeshFromRasters"
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
singularity exec ./OGS/ogs.sif createLayeredMeshFromRasters -i ./OGS/Domain2D.vtu -o ./OGS/Subsurface3D.vtu -r ./OGS/asc_layer_order_list.txt -t 10.0
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
echo "Start: Replacing Soil Element Materials by Hydrogeological Neighbor "
python3 WF_Scripts/py_paraview_replaceMATbyNeighbor.py
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
echo "Start: New MATs by element shape criteria "
python3 WF_Scripts/py_paraview_qualitybasedmaterials.py
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
echo "Start: Node Reordering"
singularity exec ./OGS/ogs.sif NodeReordering -m 1 -i ./OGS/Subsurface3D.vtu -o ./OGS/Subsurface3D.vtu
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
echo "Start: Read&Create Material Properties from CSV"
python3 WF_Scripts/py_create_OGSmaterialXMLfromCSV.py GIS_Layer_Data/Materials_Donau.csv 5 OGS/000_Media.xml
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
echo "Done"
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
echo " "
time2=$(date +"%T")


#####################################################################################
echo -e "${YELLOW}#############################################################${NC}"
echo "Start: Extract Surface for BCs "
echo -e "${YELLOW}#############################################################${NC}"
singularity exec ./OGS/ogs.sif ExtractSurface -i ./OGS/Subsurface3D.vtu -o ./OGS/Surface3D.vtu -z -1 -a 65
pvpython WF_Scripts/py_pv_calc_coordinate_correction_A.py /OGS/Surface3D.vtu
pvpython WF_Scripts/py_pv_calc_coordinate_correction_B.py /OGS/Surface3D.vtu
echo "- Done"
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
time3=$(date +"%T")

#####################################################################################
echo -e "${YELLOW}#############################################################${NC}"
echo "Start: River System Setup"
echo -e "${YELLOW}#############################################################${NC}"

cp OGS/Surface3D.vtu GIS_Rivers_Data/RiversPoints4Flux.vtu
pvpython WF_Scripts/py_pv_calc_coordinate_correction_A.py /GIS_Rivers_Data/RiversPoints4Flux.vtu
#python3 WF_Scripts/RiverNetwork_Shape2VTU.py
#pvpython WF_Scripts/RiverNetwork_VTU_MergeTimeStepsAndBlocks.py

python3 WF_Scripts/py_vtk2vtu_vtkpoints.py /GIS_Rivers_Data/DanubePoints.vtk /GIS_Rivers_Data/RiversPoints_OriginFile.vtp 
#TODO Convert to GRID

cp GIS_Rivers_Data/RiversPoints_OriginFile.vtu GIS_Rivers_Data/RiversPoints.vtu
singularity exec ./OGS/ogs.sif AssignRasterDataToMesh -i ./GIS_Rivers_Data/RiversPoints.vtu -r ./GIS_Layer_Data/DeutschlandDEM.asc -o ./GIS_Rivers_Data/RiversPoints.vtu -n -s elevation
pvpython WF_Scripts/py_pv_calc_coordinate_correction_B.py /GIS_Rivers_Data/RiversPoints.vtu
pvpython WF_Scripts/py_pv_remove_overlapping_riverpoints.py /OGS/Surface3D.vtu /GIS_Rivers_Data/RiversPoints.vtu
cp GIS_Rivers_Data/RiversPoints.vtu OGS/BC_RiverLakes_Deutschland.vtu
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
echo " - searching and removing elements out of range of domain"
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
pvpython WF_Scripts/py_pv_calc_coordinate_correction_A.py /OGS/Surface3D.vtu
pvpython WF_Scripts/py_pv_calc_coordinate_correction_B.py /OGS/Surface3D.vtu
echo " - correction of river steepness"
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
pvpython WF_Scripts/py_pv_point2rivernetwork_delaunay_based.py
pvpython WF_Scripts/py_pv_calc_coordinate_correction_B.py /OGS/BC_RiverLakes_Deutschland.vtu
python3 WF_Scripts/py_vtk_correct_river_steepness.py
pvpython WF_Scripts/py_pv_convert2pointcloud.py
pvpython WF_Scripts/py_pv_calc_coordinate_correction_B.py /OGS/BC_RiverLakes_Deutschland.vtu
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
echo "- Done"
echo -e "${YELLOW}-------------------------------------------------------------${NC}"


echo -e "${YELLOW}-------------------------------------------------------------${NC}"
echo "- Creating additional output geometries"
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
pvpython WF_Scripts/py_transfer_BC_to_OUTput.py 

echo "- Done"
echo -e "${YELLOW}-------------------------------------------------------------${NC}"

time4=$(date +"%T")

#####################################################################################

echo -e "${YELLOW}#############################################################${NC}"
echo "Start: Recharge Setup"
echo -e "${YELLOW}#############################################################${NC}"
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
echo "- NCO generate monthly daily recharge per year"
echo -e "${YELLOW}-------------------------------------------------------------${NC}"


#ncra -O --mro -d time,,,10950,10950 ./GIS_Recharge_Data/mHM/germanwide_recharge/recharge_GDM_DE1_1951-2019.nc ./GIS_Recharge_Data/mHM/recharge_30years_dailymean__mm_per_day.nc
#cdo monmean ./GIS_Recharge_Data/mHM/germanwide_recharge/recharge_GDM_DE1_1951-2019.nc ./GIS_Recharge_Data/mHM/recharge_monthly_dailymean__mm_per_day.nc
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
echo "- GDAL reprojection / warp data"
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
#gdalwarp -overwrite -s_srs EPSG:4326 -t_srs EPSG:25832 -of GTiff ./GIS_Recharge_Data/mHM/germanwide_recharge/recharge_30years_dailymean__mm_per_day.nc ./GIS_Recharge_Data/mHM/germanwide_recharge/recharge_30years_dailymean__mm_per_day.tif
#gdalwarp -overwrite -s_srs EPSG:4326 -t_srs EPSG:25832 -of GTiff ./GIS_Recharge_Data/mHM/germanwide_recharge/recharge_monthly_dailymean__mm_per_day.nc ./GIS_Recharge_Data/mHM/germanwide_recharge/recharge_monthly_dailymean__mm_per_day.tif
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
echo "- Creating OGS Recharge Boundary Condition"
echo -e "${YELLOW}-------------------------------------------------------------${NC}"

time5=$(date +"%T")

#GermanWide mHM Data
#cp ./GIS_Recharge_Data/mHM/germanwide_recharge/recharge_30years_dailymean__mm_per_day.tif ./GIS_Recharge_Data/mHM/recharge_30years_dailymean__mm_per_day.tif
#cp ./GIS_Recharge_Data/mHM/germanwide_recharge/recharge_monthly_dailymean__mm_per_day.tif ./GIS_Recharge_Data/mHM/recharge_monthly_dailymean__mm_per_day.tif

#Regional mHM Data
cp ./GIS_Recharge_Data/mHM/regional_recharge/$station_number/output_monthly/recharge_30years_dailymean__mm_per_day.tif ./GIS_Recharge_Data/mHM/recharge_30years_dailymean__mm_per_day.tif
cp ./GIS_Recharge_Data/mHM/regional_recharge/$station_number/output_monthly/recharge_monthly_dailymean__mm_per_day.tif ./GIS_Recharge_Data/mHM/recharge_monthly_dailymean__mm_per_day.tif


pvpython WF_Scripts/py_workflow_NETCDF2GEOTIFF2OGS_SteadyState.py
sed -i "s/Band /Band/g" OGS/BC_Recharge_Steady.vtu 
sed -i "s/Float32/Float64/g" OGS/BC_Recharge_Steady.vtu
python3 WF_Scripts/py_vtu_readwrite_SteadyState.py
pvpython WF_Scripts/py_pv_remove_ghost.py /OGS/BC_Recharge_Steady.vtu

pvpython WF_Scripts/py_workflow_NETCDF2GEOTIFF2OGS_TransientState.py
sed -i 's/Band /Band/g' OGS/BC_Recharge_Transient.vtu 
sed -i 's/Float32/Float64/g' OGS/BC_Recharge_Transient.vtu
python3 WF_Scripts/py_generate_ogs_recharge_and_time_setting_transientState.py 
python3 WF_Scripts/py_vtu_readwrite_TransientState.py 
pvpython WF_Scripts/py_pv_remove_ghost.py /OGS/BC_Recharge_Transient.vtu
echo "- Done"
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
echo " "

time6=$(date +"%T")


#####################################################################################
echo "- Z-Coordinate Transformation"
pvpython WF_Scripts/py_pv_calc_coordinate_correction_B.py /OGS/OUT_Rivers.vtu
pvpython WF_Scripts/py_pv_calc_coordinate_correction_B.py /OGS/OUT_Recharge.vtu
pvpython WF_Scripts/py_pv_calc_coordinate_correction_B.py /OGS/BC_RiverLakes_Deutschland.vtu
pvpython WF_Scripts/py_pv_calc_coordinate_correction_B.py /OGS/BC_Recharge_Steady.vtu
pvpython WF_Scripts/py_pv_calc_coordinate_correction_B.py /OGS/BC_Recharge_Transient.vtu
echo "- Done"
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
echo " "
echo " "

#####################################################################################
echo " "
echo -e "${YELLOW}-------------------------------------------------------------${NC}"
echo "Start OpenGeoSys Simulation for a STEADY-State Simulation"
echo -e "${YELLOW}-------------------------------------------------------------${NC}"

time7=$(date +"%T")
#####################################################################################


#singularity exec ./OGS/ogs.sif ogs -o ./OGS/ResultsSteadyState ./OGS/000_ModelRun.prj
#cp OGS/ResultsSteadyState/MOD_01_Subsurface3D_ts_100_t_3153600000000.000000.vtu OGS/Subsurface3D_IC.vtu
cd OGS
singularity exec ogs.sif identifySubdomains -f -m Subsurface3D.vtu -s 0.1 BC_Recharge_Transient.vtu
singularity exec ogs.sif identifySubdomains -f -m Subsurface3D.vtu -s 0.1 BC_RiverLakes_Deutschland.vtu
singularity exec ogs.sif identifySubdomains -f -m Subsurface3D.vtu -s 0.1 OUT_Recharge.vtu
singularity exec ogs.sif identifySubdomains -f -m Subsurface3D.vtu -s 0.1 OUT_Rivers.vtu
singularity exec ogs.sif identifySubdomains -f -m Subsurface3D.vtu -s 0.1 Surface3D.vtu

./sh_mpirun_steadystate.sh 8
time8=$(date +"%T")

cd ..

time9=$(date +"%T")

#####################################################################################

echo -e "${YELLOW}#############################################################${NC}"
echo "Start OpenGeoSys Simulation for a TRANSIENT-State Simulation"
echo -e "${YELLOW}#############################################################${NC}"

#singularity exec ./OGS/ogs.sif ogs -o ./OGS/ResultsTransientState ./OGS/001_ModelRun.prj
cd OGS
singularity exec ogs.sif RemoveGhostData -i ResultsSteadyState/MOD_01_Subsurface3D_ts_100_t_3153600000000_000000.pvtu  -o Subsurface3D_IC.vtu
singularity exec ogs.sif identifySubdomains -f -m Subsurface3D_IC.vtu -s 0.1 BC_Recharge_Transient.vtu
singularity exec ogs.sif identifySubdomains -f -m Subsurface3D_IC.vtu -s 0.1 BC_RiverLakes_Deutschland.vtu
singularity exec ogs.sif identifySubdomains -f -m Subsurface3D_IC.vtu -s 0.1 OUT_Recharge.vtu
singularity exec ogs.sif identifySubdomains -f -m Subsurface3D_IC.vtu -s 0.1 OUT_Rivers.vtu
singularity exec ogs.sif identifySubdomains -f -m Subsurface3D_IC.vtu -s 0.1 Surface3D.vtu

./sh_mpirun_transient.sh 8
cd ..
time10=$(date +"%T")

echo -e "${YELLOW}#############################################################${NC}"
echo "Moving result to Archive_00 Folder $1"
echo -e "${YELLOW}#############################################################${NC}"
mv "OGS/ResultsSteadyState" "OGS/$achive_name/$1/ResultsSteadyState"
mv "OGS/ResultsTransientState" "OGS/$achive_name/$1/ResultsTransientState"


echo " "
#pvpython WF_Scripts/py_vtu_readwrite_recharge4visualisation.py 
mv "OGS/ResultsRechargeInput" "OGS/$achive_name/$1/ResultsRechargeInput"
time11=$(date +"%T")
echo -e "${YELLOW}#############################################################${NC}"
echo -e "${YELLOW}#############################################################${NC}"
echo "END OF WORKFLOW"
echo "Start time: $time0"
echo "Step1 done: $time1"
echo "Step2 done: $time2"
echo "Step3 done: $time3"
echo "Step4 done: $time4"
echo "Step5 done: $time5"
echo "Step6 done: $time6"
echo "Step7 done: $time7"
echo "Step8 done: $time8"
echo "Step9 done: $time9"
echo "Step10 done: $time10"
echo "Step11 done: $time11"
echo -e "${YELLOW}#############################################################${NC}"





