#!/usr/bin/env bash

# Include input files
input_check2d_mesh="$1" # relates to the Check2DMeshrule output (prototcoll must exist), rules.Check2DMesh.output[0]
input_vtu="$2" # using VTU e.g. at Hydrogeology/Domain2D.vtu
input_asc_raster="$3" # using ASC e.g. at Raster Data at BGR_Data/Germany_MAT_BGR.asc
input_dem="$4" # using DEM e.g. at Hydrogeology/DeutschlandDEM.asc
input_asc_list="$5" # link to the asclist=ASC_LAYER_ORDER_LIST_TXT

# Output files
output_vtu="$6" # generate VTU i.e. Hydrogeology/Subsurface3D.vtu
output_protocol="$7" # generate Protocol File, i.e. Hydrogeology/createLayeredMeshFromRastersProtocol.txt

# Choose the layering mode
layering_mode="$8"

# Run standard layer creation based on ASC list
run_create_layered_mesh_from_rasters() {
    apptainer exec ./Programs/bin/ogs_mpi.sif createLayeredMeshFromRasters -i "$input_vtu" -o "$output_vtu" -r "$input_asc_list" -t 1.0 > "$output_protocol"
}

# Create simple 2-layer model (test version)
run_2_layered_model() {
    apptainer exec ./Programs/bin/ogs_mpi.sif AssignRasterDataToMesh -i "$input_vtu" -r "$input_asc_raster" -o "$output_vtu" -c -s MaterialIDs_BGR
    apptainer exec ./Programs/bin/ogs_mpi.sif convertVtkDataArrayToVtkDataArray -i "$output_vtu" -o "$output_vtu" -e MaterialIDs_BGR -n MaterialIDs -t int
    apptainer exec ./Programs/bin/ogs_mpi.sif AssignRasterDataToMesh -i "$output_vtu" -r "$input_dem" -o "$output_vtu" -s elevation
    apptainer exec ./Programs/bin/ogs_mpi.sif NodeReordering -i "$output_vtu" -o "$output_vtu"
    python3 Programs/scripts/python/copyElevationArrayToZCoordinates.py "$output_vtu" "$output_vtu"
    apptainer exec ./Programs/bin/ogs_mpi.sif AddLayer --copy-material-ids --add-layer-on-bottom -t 100.00 -i "$output_vtu" -o "$output_vtu"
    apptainer exec ./Programs/bin/ogs_mpi.sif AddLayer --add-layer-on-bottom -t 500.00 -i "$output_vtu" -o "$output_vtu"
    apptainer exec ./Programs/bin/ogs_mpi.sif removeMeshElements -i "$output_vtu" -o "$output_vtu" -t tri
    apptainer exec ./Programs/bin/ogs_mpi.sif checkMesh $output_vtu > $output_protocol
    apptainer exec ./Programs/bin/ogs_mpi.sif editMaterialID -r -m 99 -n 13 -i "$output_vtu" -o "$output_vtu"
    #apptainer exec ./Programs/bin/ogs_mpi.sif editMaterialID -r -m 5 -n 14 -i "$output_vtu" -o "$output_vtu"
}

# Check which option to use
if [ "$layering_mode" == "Create_Layered_Mesh_From_Raster_Data" ]; then
    run_create_layered_mesh_from_rasters
elif [ "$layering_mode" == "Create_2_Layer_BGR_Model" ]; then
    run_2_layered_model
else
    echo "Invalid option. Use 'Create_Layered_Mesh_From_Raster_Data' for standard run_create_layered_mesh_from_rasters or 'Create_2_Layer_BGR_Model' for the simplified 2-layer model."
fi
