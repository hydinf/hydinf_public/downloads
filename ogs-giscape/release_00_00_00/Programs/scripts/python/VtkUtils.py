#!/usr/bin/env python3

from vtk import *


def readVtkMesh(file_path):
    """
    Reads VTU file and returns a vtk unstructured grid.

    Parameters:
    file_path (str): Path to the VTU file.

    Returns:
    vtk.vtkUnstructuredGrid: The unstructured grid from the VTU file.
    """
    reader = vtkXMLUnstructuredGridReader()
    reader.SetFileName(file_path)
    reader.Update()
    return reader.GetOutput()


def writeVtkMesh(file_name, mesh):
    """
    write the given vtk mesh to the file
    """
    w = vtkXMLUnstructuredGridWriter()
    w.SetFileName(file_name)
    w.SetInputData(mesh)
    w.SetCompressorTypeToNone()
    w.Update()
