
#rm ./OGS/*.vtu
rm ./OGS/*.mesh
rm ./OGS/*.bin

rm -rf ./OGS/ResultsSteadyState
mkdir ./OGS/ResultsSteadyState

rm -rf ./OGS/ResultsTransientState
mkdir ./OGS/ResultsTransientState

rm -rf ./OGS/ResultsRechargeInput
mkdir ./OGS/ResultsRechargeInput


