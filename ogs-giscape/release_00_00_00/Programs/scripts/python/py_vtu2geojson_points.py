"""
This module converts VTU (VTK Unstructured Grid) files to GeoJSON format.
It reads VTU files, extracts point data, and outputs this data in GeoJSON format,
specifying a Coordinate Reference System (CRS).

Usage:
    Run the script from the command line by providing paths to the input VTU file and the output GeoJSON file.
    Example:
        python py_vtu_to_geojson.py input.vtu output.geojson
"""


import sys
import vtk
import json

def convert_vtu_to_geojson(vtu_file_path, geojson_file_path, crs_properties):
    """
    Converts a VTU file to GeoJSON format.

    Args:
        vtu_file_path (str): Path to the input VTU file.
        geojson_file_path (str): Path to the output GeoJSON file.
        crs_properties (dict): Properties of the Coordinate Reference System for the GeoJSON.

    Outputs:
        GeoJSON file with points extracted from the VTU file.
    """
    print(f'Converting VTU file at {vtu_file_path} to GeoJSON at {geojson_file_path}.')

    # Initialize the VTK reader and read the VTU file
    reader = vtk.vtkXMLUnstructuredGridReader()
    reader.SetFileName(vtu_file_path)
    reader.Update()

    # Extract points from the VTU file
    points = reader.GetOutput().GetPoints().GetData()
    point_list = [
        {
            'type': 'Feature',
            'geometry': {
                'type': 'Point',
                'coordinates': list(points.GetTuple(i))
            }
        } for i in range(points.GetNumberOfTuples())
    ]

    # Define the GeoJSON structure
    feature_collection = {
        'type': 'FeatureCollection',
        'crs': {
            'type': 'name',
            'properties': crs_properties
        },
        'features': point_list
    }

    # Write the GeoJSON data to a file
    with open(geojson_file_path, 'w') as outfile:
        json.dump(feature_collection, outfile)

    print(f'GeoJSON file has been created at {geojson_file_path}')

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python py_vtu_to_geojson.py <input_vtu_path> <output_geojson_path>")
    else:
        input_vtu = sys.argv[1]
        output_geojson = sys.argv[2]
        crs_properties = {
            "name": "urn:ogc:def:crs:EPSG::25832"  # Example EPSG code for WGS84 Geographic Coordinate System
        }
        convert_vtu_to_geojson(input_vtu, output_geojson, crs_properties)

