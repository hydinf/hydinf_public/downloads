import sys
import os
import vtk
from vtk.util.numpy_support import vtk_to_numpy
import numpy as np

import VtkUtils

def get_full_path(relative_path):
    """
    Constructs the full path for a file based on the current working directory and a given relative path.

    Parameters:
    relative_path (str): The relative path to the file.

    Returns:
    str: The absolute path to the file.
    """
    return os.path.join(os.getcwd(), relative_path)


def calculate_triangle_areas(ugrid):
    """
    Calculates the area of each triangle in an unstructured grid.

    Parameters:
    ugrid (vtk.vtkUnstructuredGrid): The unstructured grid containing the mesh.

    Returns:
    list: List of areas for each triangle.
    """
    areas = []
    for i in range(ugrid.GetNumberOfCells()):
        cell = ugrid.GetCell(i)
        pts = cell.GetPoints()
        p1, p2, p3 = (np.array(pts.GetPoint(j)) for j in range(3))
        areas.append(0.5 * np.linalg.norm(np.cross(p2 - p1, p3 - p1)) / 3)
    return areas

def calculate_node_areas(ugrid, areas):
    """
    Calculates the sum of adjacent triangle areas for each node.

    Parameters:
    ugrid (vtk.vtkUnstructuredGrid): The unstructured grid containing the mesh.
    areas (list): Areas of each triangle.

    Returns:
    numpy.ndarray: Array of summed areas for each node.
    """
    node_area_sum = np.zeros(ugrid.GetPoints().GetNumberOfPoints())
    for i in range(ugrid.GetNumberOfCells()):
        cell = ugrid.GetCell(i)
        for j in range(cell.GetNumberOfPoints()):
            node_id = cell.GetPointId(j)
            node_area_sum[node_id] += areas[i]
    return node_area_sum

def save_vtu_with_node_areas(ugrid, node_area_sum, output_file_path):
    """
    Saves a VTU file with node area data.

    Parameters:
    ugrid (vtk.vtkUnstructuredGrid): The unstructured grid to save.
    node_area_sum (numpy.ndarray): Node area data to be added to the grid.
    output_file_path (str): Path to save the output VTU file.
    """
    point_data = ugrid.GetPointData()
    array = vtk.vtkFloatArray()
    array.SetNumberOfValues(len(node_area_sum))
    for i, value in enumerate(node_area_sum):
        array.SetValue(i, value)
    array.SetName("NodeArea")
    point_data.AddArray(array)

    writer = vtk.vtkXMLUnstructuredGridWriter()
    writer.SetFileName(output_file_path)
    writer.SetInputData(ugrid)
    writer.Write()
    print(f"VTU file with node areas saved as {output_file_path}")

def main():
    """
    Main function to execute the VTU processing and area calculation workflow.
    """
    if len(sys.argv) < 2:
        print("Usage: python script.py <input_vtu_file>")
        sys.exit(1)

    vtu_file_path = get_full_path(sys.argv[1])
    vtu_file_name_out = sys.argv[2]

    ugrid = VtkUtils.readVtkMesh(vtu_file_path)
    areas = calculate_triangle_areas(ugrid)
    node_area_sum = calculate_node_areas(ugrid, areas)
    save_vtu_with_node_areas(ugrid, node_area_sum, vtu_file_name_out)

if __name__ == "__main__":
    main()

