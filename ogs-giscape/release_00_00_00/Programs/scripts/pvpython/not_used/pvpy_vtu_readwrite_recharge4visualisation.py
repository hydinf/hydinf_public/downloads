import sys
import os
from vtk import *
from paraview.simple import *
##########################################################################################
# files and parameters
#file_path = os.path.abspath(os.path.dirname(__file__))

recharge_input_file = str(sys.argv[1])
recharge_output_file = str(sys.argv[2])
recharge_pvd_file = str(sys.argv[3])
subfolder = str(sys.argv[4])

file_path = os.getcwd()
#print (file_path)

vtu_file_name = file_path + '/' + recharge_input_file
vtu_file_name_out = file_path + '/' + recharge_output_file
pvd_file_name = file_path + '/' + recharge_pvd_file 
#vtu_file_name = file_path + '/OGS/BC_Recharge_Transient.vtu'
#vtu_file_name_out = file_path +'/OGS/ResultsRechargeInput/Recharge.vtu'
#pvd_file_name = file_path + '/OGS/ResultsRechargeInput/Recharge.pvd'

print ('Reading: ')    
print (vtu_file_name)

##########################################################################################
# create a new 'XML Unstructured Grid Reader'
rechargevtu = XMLUnstructuredGridReader(registrationName='Recharge.vtu', FileName=[vtu_file_name])

# Properties modified on rechargevtu
rechargevtu.PointArrayStatus = []
rechargevtu.TimeArray = 'None'

# update the view to ensure updated data information
#renderView1.Update()

reader = vtkXMLUnstructuredGridReader() #VTU Reader
reader.SetFileName(vtu_file_name)
reader.Update() 
mesh = reader.GetOutput()
all_attributes = mesh.GetAttributesAsFieldData(1)
timestep = 0
timestep_size = 2628000
band_counter = 0

with open(pvd_file_name, 'w') as f:
    f.write('<?xml version="1.0"?>\n')
    f.write('<VTKFile type="Collection" version="0.1" byte_order="LittleEndian" compressor="vtkZLibDataCompressor">\n')
    f.write('  <Collection>\n')
    
    # Zähle die Bänder
    for i in range(all_attributes.GetNumberOfComponents()):
     name = all_attributes.GetArrayName(i)
     if name.find('Band') != -1 :
      band_counter = band_counter +1
      rechargevtu.CellArrayStatus = [name]
      calculator1 = Calculator(registrationName='Calculator1', Input=rechargevtu)
      calculator1.AttributeType = 'Cell Data'
      calculator1.ResultArrayName = 'Recharge'
      calculator1.Function = name
      vtu_name = "Recharge_ts_"+str(band_counter-1)+"_t_"+str(timestep)+".000000.vtu"
      f.write('    <DataSet timestep="' + str(timestep) + '" group="" part="0" file="'+str(vtu_name)+'"/>\n')
      SaveData(file_path + '/'+ subfolder +'/'+ vtu_name, proxy=calculator1, ChooseArraysToWrite=1, CellDataArrays=['Recharge'], DataMode='Binary')
      timestep = timestep + timestep_size
      print (vtu_name)
    f.write('  </Collection>\n')
    f.write('</VTKFile>\n')

  
vtu_name = "Recharge_ts_"+str(band_counter)+"_t_"+str(timestep)+".000000.vtu"
SaveData(file_path + '/'+ subfolder +'/'+ vtu_name, proxy=calculator1, ChooseArraysToWrite=1, CellDataArrays=['Recharge'], DataMode='Binary')


print (vtu_name)
print ('*******************************************')
print ('Number of Bands found: ' + str(band_counter))









