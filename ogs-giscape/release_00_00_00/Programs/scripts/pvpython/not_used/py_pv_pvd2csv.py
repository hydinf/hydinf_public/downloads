#Clipping by Thomas Kalbacher, UFZ, Leipzig, Germany
##########################################################################################
import sys
import os
from datetime import datetime
from vtk import *
from paraview.simple import *
import numpy as np
##########################################################################################
file_path = os.getcwd()
#-----------------------------------------------------------------------------------------
vtu_input_filename = str(sys.argv[1])
csv_output_filename = str(sys.argv[2])
print('. . . Converting VTU Time Series to CSV Time Series - Heads only')

#DomainVTU = vtkXMLPUnstructuredGridReader(registrationName='Domain', FileName=[vtu_input_filename])
DomainVTU = PVDReader(registrationName='Domain', FileName=[vtu_input_filename])
DomainVTU.PointArrays = ['head']

print ('Saving File: ' + str (csv_output_filename))
# save data
SaveData(csv_output_filename, proxy=DomainVTU,WriteTimeSteps=1, ChooseArraysToWrite=1, PointDataArrays=['head'], Precision=10, UseScientificNotation=1, AddMetaData=0, AddTime=1)
