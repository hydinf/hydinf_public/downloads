#!/usr/bin/env bash

path=OGS_Results/Abens_Debug01/SteadyState/02/
transient_path=OGS_Results/Abens_Debug01/Transient/02/

function checkFilesExist()
{
    files=$2
    for file_to_check in "${files[@]}";
    do
        # check if expected output files exist
        if [ ! -f $1/${file_to_check} ]; then
            echo "File $1/${file_to_check} not found!"
            exit 1
        fi
    done
}

declare -a steady_state_files_to_check=(
    'MOD_01_Subsurface3D_ts_10_t_315360000000_000000.pvtu'
    'MOD_01_OUT_Recharge_ts_10_t_315360000000_000000.pvtu'
    'MOD_01_OUT_Rivers_ts_10_t_315360000000_000000.pvtu'
    )
declare -a transient_files_to_check=(
    'MOD_01_OUT_Rivers_ts_863_t_2267964000_000000.pvtu'
    'MOD_01_OUT_Recharge_ts_863_t_2267964000_000000.pvtu'
    'MOD_01_Surface3D_ts_863_t_2267964000_000000.pvtu'
    )

checkFilesExist ${path} "${steady_state_files_to_check[@]}"
checkFilesExist ${transient_path} "${transient_files_to_check[@]}"
exit 0
