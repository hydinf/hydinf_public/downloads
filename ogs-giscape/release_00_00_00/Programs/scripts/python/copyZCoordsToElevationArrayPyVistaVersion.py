#!/usr/bin/env python3

from vtk import *
from vtk.util import numpy_support
from sys import argv, exit

import numpy as np
import pyvista as pv

def vtkToPyvista(vtk_grid):
    # Convert VTK unstructured grid to PyVista mesh
    pyvista_mesh = pv.UnstructuredGrid(vtk_grid)

    return pyvista_mesh

def copyZCoordsToElevationArray(mesh):
    # Get points and extract z-coordinates
    points = mesh.points
    z_coords = points[:, 2]

    # Create a new array for elevation
    elevation = np.empty(mesh.n_points)
    elevation[:] = z_coords

    # Reset z-coordinates
    points[:, 2] = 0.0

    # Add elevation array to point data
    mesh.point_data['elevation'] = elevation

    return mesh

def readVtkMesh(file_name):
    reader = vtkXMLUnstructuredGridReader()
    reader.SetFileName(file_name)
    reader.Update()
    return reader.GetOutput()

def main(args):
    input_file = args[1]
    output_file = args[2]

    m = readVtkMesh(input_file)
    py_vista_mesh = vtkToPyvista(m)
    copyZCoordsToElevationArray(py_vista_mesh)
    py_vista_mesh.save(output_file)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage: " + str(sys.argv[0]) + " input.vtu output.vtu")
        exit(1)

    main(sys.argv)
