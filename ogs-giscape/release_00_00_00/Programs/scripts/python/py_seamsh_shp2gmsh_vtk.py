import sys
import os
import geopandas as gpd
from shapely.geometry import Point, box
from osgeo import osr
import numpy as np
import seamsh
from seamsh.geometry import CurveType

def construct_file_paths():
    """
    Constructs full paths for all input and output files based on command line arguments.
    """
    file_path = os.getcwd()
    return {
        "boundary": os.path.join(file_path, str(sys.argv[1])),
        "rivers": os.path.join(file_path, str(sys.argv[2])),
        "wells": os.path.join(file_path, str(sys.argv[3])),
        "rivers_output": os.path.join(file_path, str(sys.argv[4])),
        "wells_output": os.path.join(file_path, str(sys.argv[5])),
        "vtk_output": os.path.join(file_path, str(sys.argv[6]))
    }

def clip_shapefiles(river_file, boundary_file, output_file):
    """
    Clips river shapefiles to the boundary shapefile and saves the output.
    """
    river_shp = gpd.read_file(river_file)
    boundary_shp = gpd.read_file(boundary_file)
    bbox = boundary_shp.geometry.total_bounds
    bbox_polygon = box(*bbox)
    clipped_rivers = river_shp[river_shp.geometry.intersects(bbox_polygon)]
    clipped_rivers.to_file(output_file)
    print("1st river network clipping - done")

def clip_and_reduce(input_file, output_file, boundary_file):
    """
    Performs data reduction by clipping points within the boundary.
    """
    point_file = gpd.read_file(input_file)
    boundary_shp = gpd.read_file(boundary_file)
    polygons = list(boundary_shp.geometry)
    within_polygons = []
    
    for i, point in point_file.iterrows():
        if Point(point.geometry).within(polygons):
            within_polygons.append(i)
    points_within_polygons = point_file.loc[within_polygons]
    points_within_polygons.to_file(output_file)
    print("2nd river network clipping & data reduction - done")

def generate_mesh(files, mesh_size_min, mesh_size_max):
    """
    Generates a mesh using GMSH based on the specified domain and settings.
    """
    domain_srs = osr.SpatialReference()
    domain_srs.ImportFromEPSG(25832)
    domain = seamsh.geometry.Domain(domain_srs)
    domain.add_boundary_curves_shp(files['boundary'], None, CurveType.POLYLINE)
    domain.add_interior_points_shp(files['rivers_output'], None)
    domain.add_interior_points_shp(files['wells_output'], None)
    
    dist_boundary = seamsh.field.Distance(domain, mesh_size_max)
    def mesh_size(x, projection):
        return np.clip(dist_boundary(x, projection), mesh_size_min, mesh_size_max)
    
    output_srs = osr.SpatialReference()
    output_srs.ImportFromEPSG(25832)
    seamsh.gmsh.gmsh.option.setNumber("Mesh.Algorithm", 8)
    seamsh.gmsh.gmsh.option.setNumber("Mesh.MeshSizeMin", mesh_size_min)
    seamsh.gmsh.gmsh.option.setNumber("Mesh.MeshSizeMax", mesh_size_max)
    
    print("Starting with GMSH")
    seamsh.gmsh.mesh(domain, files['vtk_output'], mesh_size, intermediate_file_name="debug", output_srs=output_srs)

def main():
    """
    Main function to orchestrate the geospatial processing and mesh generation workflow.
    """
    files = construct_file_paths()
    clip_shapefiles(files['rivers'], files['boundary'], files['rivers_output'])
    clip_and_reduce(files['rivers_output'], files['rivers_output'], files['boundary'])
    clip_and_reduce(files['wells'], files['wells_output'], files['boundary'])
    generate_mesh(files, int(sys.argv[7]), int(sys.argv[8]))

if __name__ == "__main__":
    main()

