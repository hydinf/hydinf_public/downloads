#!/usr/bin/env bash

function diffVTKFiles()
{
    ref_path=$1
    result_path=$2
    local -n files=$3

    all_identical=1

    for f in "${files[@]}"
    do
        if [[ $(apptainer exec ./Programs/bin/ogs_mpi.sif vtkdiff -m ${ref_path}/${f} ${result_path}/${f}) ]];
        then
            echo -e "\u274c $f"
            all_identical=0
        else
            echo -e "\u2714 $f"
        fi
    done

    return ${all_identical}
}
