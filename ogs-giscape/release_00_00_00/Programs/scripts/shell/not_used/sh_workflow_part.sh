ncra -O --mro -d time,,,10950,10950 ./GIS_Recharge_Data/mHM/recharge_GDM_DE1_1951-2019.nc ./GIS_Recharge_Data/mHM/recharge_30years_dailymean__mm_per_day.nc

cdo monmean ./GIS_Recharge_Data/mHM/recharge_GDM_DE1_1951-2019.nc ./GIS_Recharge_Data/mHM/recharge_monthly_dailymean__mm_per_day.nc

gdalwarp -overwrite -s_srs EPSG:4326 -t_srs EPSG:25832 -of GTiff ./GIS_Recharge_Data/mHM/recharge_30years_dailymean__mm_per_day.nc ./GIS_Recharge_Data/mHM/recharge_30years_dailymean__mm_per_day.tif
gdalwarp -overwrite -s_srs EPSG:4326 -t_srs EPSG:25832 -of GTiff ./GIS_Recharge_Data/mHM/recharge_monthly_dailymean__mm_per_day.nc ./GIS_Recharge_Data/mHM/recharge_monthly_dailymean__mm_per_day.tif
