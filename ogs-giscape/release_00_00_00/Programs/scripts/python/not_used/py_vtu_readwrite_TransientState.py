import sys
import os
from vtk import *
##########################################################################################
# files and parameters
#file_path = os.path.abspath(os.path.dirname(__file__))
file_path = os.getcwd()
#print (file_path)
vtu_file_name = file_path + '/OGS/BC_Recharge_Transient.vtu'
vtu_file_name_out = file_path +'/OGS/BC_Recharge_Transient.vtu'

print ('Converting ASCII into BINARY: ')    
print (vtu_file_name)

##########################################################################################
reader = vtkXMLUnstructuredGridReader() #VTU Reader
reader.SetFileName(vtu_file_name)
reader.Update() # Needed because of GetScalarRange
mesh = reader.GetOutput()
scalar_range = mesh.GetScalarRange()
cellIds = vtkIdList()
##########################################################################################
extract = vtkExtractUnstructuredGridPiece()
extract.SetInputConnection(reader.GetOutputPort())
cellData = mesh.GetCellData()
writer = vtkXMLUnstructuredGridWriter()
writer.SetFileName(vtu_file_name_out)
writer.SetDataModeToBinary()
writer.SetInputConnection(extract.GetOutputPort())
writer.Write()


