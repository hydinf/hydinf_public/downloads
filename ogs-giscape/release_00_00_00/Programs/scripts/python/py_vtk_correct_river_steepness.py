import sys
import os
from math import *
from vtk import *

def process_vtu_file():
    # Define input and output file paths using command line arguments
    relative_input_file_path = str(sys.argv[1])
    file_path = os.getcwd()

    # Construct full paths for input and output files
    vtu_file_name = file_path + relative_input_file_path
    vtu_file_name_out = vtu_file_name

    # Initialize VTK XML unstructured grid reader
    reader = vtkXMLUnstructuredGridReader()
    reader.SetFileName(vtu_file_name)
    reader.Update()  # Needed for GetScalarRange
    mesh = reader.GetOutput()
    scalar_range = mesh.GetScalarRange()
    cellIds = vtkIdList()
    cellIds0 = vtkIdList()
    cellpointIds0 = vtkIdList()
    coordinates = [0.0, 0.0, 0.0]
    iq = vtkMeshQuality()
    iq.SetInputConnection(reader.GetOutputPort())

    # Access Data
    print(". . . . . . . . . .Collecting Element Information:")
    # Elements
    numberofcells = mesh.GetNumberOfCells()
    numberofpoints = mesh.GetNumberOfPoints()
    print("Processing " + str(numberofcells) + " elements")
    cell_data = mesh.GetCellData().GetScalars("steepness")
    point_data = mesh.GetPointData().GetScalars("waterelevation")
    pnts = mesh.GetPoints()

    # Setup Data
    defined_max_steepness = 0.033

    # While loop to correct steepness
    targethit = 1000
    while targethit >= 1:
        print(targethit)
        targethit = 0
        print('. . . . . correction mode iteration')

        # Loop through cells
        for cell_id in range(numberofcells):
            cell = mesh.GetCell(cell_id)
            cell_type = cell.GetCellType()

            # Check if the cell is a triangle (element type 5)
            if cell_type == 5:
                pointdata = mesh.GetCellPoints(cell_id, cellpointIds0)
                number_of_cell_points = cellpointIds0.GetNumberOfIds()
                allpoints = mesh.GetPoints()

                if number_of_cell_points == 3:
                    elevation_0 = point_data.GetTuple1(cellpointIds0.GetId(0))
                    elevation_1 = point_data.GetTuple1(cellpointIds0.GetId(1))
                    elevation_2 = point_data.GetTuple1(cellpointIds0.GetId(2))

                    point_coordinates_0 = mesh.GetPoint(cellpointIds0.GetId(0))
                    point_coordinates_1 = mesh.GetPoint(cellpointIds0.GetId(1))
                    point_coordinates_2 = mesh.GetPoint(cellpointIds0.GetId(2))

                    horizontal_dist0 = sqrt((point_coordinates_1[0] - point_coordinates_0[0])**2 + 
                                            (point_coordinates_1[1] - point_coordinates_0[1])**2)
                    horizontal_dist1 = sqrt((point_coordinates_2[0] - point_coordinates_0[0])**2 + 
                                            (point_coordinates_2[1] - point_coordinates_0[1])**2)
                    horizontal_dist2 = sqrt((point_coordinates_1[0] - point_coordinates_2[0])**2 + 
                                            (point_coordinates_1[1] - point_coordinates_2[1])**2)
                    vertical_dist0 = sqrt((point_coordinates_1[2] - point_coordinates_0[2])**2)
                    vertical_dist1 = sqrt((point_coordinates_2[2] - point_coordinates_0[2])**2)
                    vertical_dist2 = sqrt((point_coordinates_1[2] - point_coordinates_2[2])**2)

                    steepness0 = 0 if horizontal_dist0 == 0 else vertical_dist0 / horizontal_dist0
                    steepness1 = 0 if horizontal_dist1 == 0 else vertical_dist1 / horizontal_dist1
                    steepness2 = 0 if horizontal_dist2 == 0 else vertical_dist2 / horizontal_dist2

                    steepness_max_val = max(steepness0, steepness1, steepness2)
                    z_coor_0 = point_coordinates_0[2]
                    z_coor_1 = point_coordinates_1[2]
                    z_coor_2 = point_coordinates_2[2]

                    if steepness0 >= defined_max_steepness:
                        if z_coor_0 >= z_coor_1:
                            z_coor_0 = z_coor_1 + (0.5 * defined_max_steepness * horizontal_dist0)
                        if z_coor_0 <= z_coor_1:
                            z_coor_1 = z_coor_0 + (0.5 * defined_max_steepness * horizontal_dist0)
                        targethit = targethit + 1

                    if steepness1 >= defined_max_steepness:
                        if z_coor_0 >= z_coor_2:
                            z_coor_0 = z_coor_2 + (0.5 * defined_max_steepness * horizontal_dist1)
                        if z_coor_0 <= z_coor_2:
                            z_coor_2 = z_coor_0 + (0.5 * defined_max_steepness * horizontal_dist1)
                        targethit = targethit + 1

                    if steepness2 >= defined_max_steepness:
                        if z_coor_2 >= z_coor_1:
                            z_coor_2 = z_coor_1 + (0.5 * defined_max_steepness * horizontal_dist2)
                        if z_coor_2 <= z_coor_1:
                            z_coor_1 = z_coor_2 + (0.5 * defined_max_steepness * horizontal_dist2)
                        targethit = targethit + 1

                    coordinates[0] = point_coordinates_0[0]
                    coordinates[1] = point_coordinates_0[1]
                    coordinates[2] = z_coor_0
                    allpoints.SetPoint(cellpointIds0.GetId(0), coordinates)
                    coordinates[0] = point_coordinates_1[0]
                    coordinates[1] = point_coordinates_1[1]
                    coordinates[2] = z_coor_1
                    allpoints.SetPoint(cellpointIds0.GetId(1), coordinates)
                    coordinates[0] = point_coordinates_2[0]
                    coordinates[1] = point_coordinates_2[1]
                    coordinates[2] = z_coor_2
                    allpoints.SetPoint(cellpointIds0.GetId(2), coordinates)
                    point_data.SetTuple1(cellpointIds0.GetId(0), z_coor_0)
                    point_data.SetTuple1(cellpointIds0.GetId(1), z_coor_1)
                    point_data.SetTuple1(cellpointIds0.GetId(2), z_coor_2)
                    mesh.SetPoints(allpoints)

            # Check if the cell is a line (element type 3)
            elif cell_type == 3:
                pointdata = mesh.GetCellPoints(cell_id, cellpointIds0)
                number_of_cell_points = cellpointIds0.GetNumberOfIds()
                allpoints = mesh.GetPoints()

                if number_of_cell_points == 2:
                    elevation_0 = point_data.GetTuple1(cellpointIds0.GetId(0))
                    elevation_1 = point_data.GetTuple1(cellpointIds0.GetId(1))
                    point_coordinates_0 = mesh.GetPoint(cellpointIds0.GetId(0))
                    point_coordinates_1 = mesh.GetPoint(cellpointIds0.GetId(1))
                    horizontal_dist0 = sqrt((point_coordinates_1[0] - point_coordinates_0[0])**2 + 
                                            (point_coordinates_1[1] - point_coordinates_0[1])**2)
                    vertical_dist0 = sqrt((point_coordinates_1[2] - point_coordinates_0[2])**2)

                    steepness0 = 0 if horizontal_dist0 == 0 else vertical_dist0 / horizontal_dist0

                    steepness_max_val = steepness0
                    z_coor_0 = point_coordinates_0[2]
                    z_coor_1 = point_coordinates_1[2]

                    if steepness0 >= defined_max_steepness:
                        if z_coor_0 >= z_coor_1:
                            z_coor_0 = z_coor_1 + (0.5 * defined_max_steepness * horizontal_dist0)
                        if z_coor_0 <= z_coor_1:
                            z_coor_1 = z_coor_0 + (0.5 * defined_max_steepness * horizontal_dist0)
                        targethit = targethit + 1

                    coordinates[0] = point_coordinates_0[0]
                    coordinates[1] = point_coordinates_0[1]
                    coordinates[2] = z_coor_0
                    allpoints.SetPoint(cellpointIds0.GetId(0), coordinates)
                    coordinates[0] = point_coordinates_1[0]
                    coordinates[1] = point_coordinates_1[1]
                    coordinates[2] = z_coor_1
                    allpoints.SetPoint(cellpointIds0.GetId(1), coordinates)
                    point_data.SetTuple1(cellpointIds0.GetId(0), z_coor_0)
                    point_data.SetTuple1(cellpointIds0.GetId(1), z_coor_1)
                    mesh.SetPoints(allpoints)

    # Reset z-coordinate for all points to zero
    for i in range(numberofpoints):
        pnt = mesh.GetPoint(i)
        coordinates[0] = pnt[0]
        coordinates[1] = pnt[1]
        coordinates[2] = 0.0
        pnts.SetPoint(i, coordinates)

    # Write the modified mesh to a VTU file
    writer = vtkXMLUnstructuredGridWriter()
    writer.SetFileName(vtu_file_name_out)
    writer.SetDataModeToBinary()
    writer.SetInputConnection(reader.GetOutputPort())
    writer.Write()

def main():
    process_vtu_file()

if __name__ == "__main__":
    main()
