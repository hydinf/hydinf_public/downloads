import sys
import os
from vtk import vtkUnstructuredGridReader, vtkXMLUnstructuredGridWriter, vtkIntArray, vtkThreshold, vtkDataObject

def construct_full_path(relative_path):
    """
    Constructs a full file path by combining the current working directory with a relative path.

    Parameters:
    relative_path (str): The relative path to a file.

    Returns:
    str: The absolute path to the file.
    """
    return os.path.join(os.getcwd(), relative_path)

def read_unstructured_grid(file_path):
    """
    Reads an unstructured grid VTU file and returns the vtkUnstructuredGrid object.

    Parameters:
    file_path (str): The path to the VTU file.

    Returns:
    vtkUnstructuredGrid: The read unstructured grid data.
    """
    reader = vtkUnstructuredGridReader()
    reader.SetFileName(file_path)
    reader.Update()
    return reader.GetOutput()

def add_vertices_count_to_cells(input_data):
    """
    Adds a new cell data array counting the number of vertices for each cell in the input data.

    Parameters:
    input_data (vtkUnstructuredGrid): The input unstructured grid data.

    Returns:
    vtkUnstructuredGrid: The unstructured grid data with the vertices count added to cell data.
    """
    num_vertices = vtkIntArray()
    num_vertices.SetName("num_vertices")
    for i in range(input_data.GetNumberOfCells()):
        cell = input_data.GetCell(i)
        num_vertices.InsertNextValue(cell.GetNumberOfPoints())
    input_data.GetCellData().AddArray(num_vertices)
    return input_data

def filter_cells_with_three_vertices(input_data):
    """
    Applies a threshold filter to extract cells that have exactly three vertices.

    Parameters:
    input_data (vtkUnstructuredGrid): The input unstructured grid data.

    Returns:
    vtkUnstructuredGrid: The filtered unstructured grid data.
    """
    threshold = vtkThreshold()
    threshold.SetInputData(input_data)
    threshold.SetInputArrayToProcess(0, 0, 0, vtkDataObject.FIELD_ASSOCIATION_CELLS, "num_vertices")
    threshold.SetLowerThreshold(3)
    threshold.SetUpperThreshold(3)
    threshold.Update()
    return threshold.GetOutput()

def write_unstructured_grid(output_data, output_file_path):
    """
    Writes the provided unstructured grid data to a file in binary mode.

    Parameters:
    output_data (vtkUnstructuredGrid): The data to write.
    output_file_path (str): The output file path.
    """
    writer = vtkXMLUnstructuredGridWriter()
    writer.SetFileName(output_file_path)
    writer.SetDataModeToBinary()
    writer.SetInputData(output_data)
    writer.Write()

def main(input_file, output_file):
    """
    Main function to process an unstructured grid VTU file, adding a vertices count and filtering cells with three vertices.

    Parameters:
    input_file (str): Relative path to the input VTU file.
    output_file (str): Relative path to the output VTU file.
    """
    print('Converting ASCII into BINARY:')
    input_path = construct_full_path(input_file)
    print(input_path)
    input_data = read_unstructured_grid(input_path)
    input_data = add_vertices_count_to_cells(input_data)
    filtered_data = filter_cells_with_three_vertices(input_data)
    output_path = construct_full_path(output_file)
    write_unstructured_grid(filtered_data, output_path)

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("Usage: python script.py <inputfile> <outputfile>")
        sys.exit(1)
    main(sys.argv[1], sys.argv[2])

