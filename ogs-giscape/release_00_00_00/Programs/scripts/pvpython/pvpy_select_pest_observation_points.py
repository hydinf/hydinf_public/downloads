#Clipping by Thomas Kalbacher, UFZ, Leipzig, Germany
##########################################################################################
import sys
import os
from datetime import datetime
from vtk import *
from paraview.simple import *
import numpy as np
##########################################################################################
# parameter specification from input arguments
realtive_path_domain = str(sys.argv[1])
realtive_path_river = str(sys.argv[2])

#skalar_type = str(sys.argv[2])
#skalar_name = str(sys.argv[3])
#LowerThreshold = float(sys.argv[4])
#UpperThreshold = float(sys.argv[5])


#skalar_type = 'POINTS'
#skalar_name = 'elevation'
#LowerThreshold = 0.0001
#UpperThreshold = 1000.0


#-----------------------------------------------------------------------------------------
# files and parameters
file_path = os.getcwd()
#-----------------------------------------------------------------------------------------
vtu_input_filename = file_path + "/" + realtive_path_domain
save_as_file_name = file_path + "/" + realtive_path_river


print('. . . Reading: ' +  str(save_as_file_name))
# create a new 'XML Unstructured Grid Reader'
DomainVTU = XMLUnstructuredGridReader(registrationName='Domain', FileName=[vtu_input_filename])
PointsVTU = XMLUnstructuredGridReader(registrationName='Points', FileName=[save_as_file_name])

#rechargeSurfacevtu.CellArrayStatus = ['bulk_element_ids', 'bulk_face_ids']
#rechargeSurfacevtu.PointArrayStatus = ['bulk_node_ids']
DomainVTU.UpdatePipeline()
PointsVTU.UpdatePipeline()


# create a new 'Calculator'
calculator1 = Calculator(registrationName='Calculator1', Input=DomainVTU)
calculator1.CoordinateResults = 1
calculator1.ResultArrayName = 'coordsZ'
calculator1.Function = 'coordsX*iHat + coordsY*jHat + (coordsZ-coordsZ) *kHat'
UpdatePipeline(time=0.0, proxy=calculator1)

# create a new 'Point Dataset Interpolator'
pointDatasetInterpolator1 = PointDatasetInterpolator(registrationName='PointDatasetInterpolator1', Input=PointsVTU, Source=calculator1)
pointDatasetInterpolator1.Kernel = 'GaussianKernel'
pointDatasetInterpolator1.Kernel.KernelFootprint = 'N Closest'
UpdatePipeline(time=0.0, proxy=pointDatasetInterpolator1)


print ('Saving File: ' + str (save_as_file_name))
# save data
SaveData(save_as_file_name, proxy=pointDatasetInterpolator1,DataMode='Binary')

SaveData(os.path.splitext(save_as_file_name)[0]+'.csv', proxy=pointDatasetInterpolator1,ChooseArraysToWrite=1, PointDataArrays=['Observation_Data'], Precision=10, UseScientificNotation=1, AddMetaData=0)

