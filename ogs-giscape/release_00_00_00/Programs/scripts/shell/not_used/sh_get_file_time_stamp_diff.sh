#!/bin/bash

# Set the base directory, adjust as needed
base_dir="."

# Find the oldest file (based on modification time)
oldest_file=$(find "$base_dir" -type f -printf '%T+ %p\n' | sort | head -n 1)

# Find the newest file
newest_file=$(find "$base_dir" -type f -printf '%T+ %p\n' | sort -r | head -n 1)

# Extract the times
oldest_time=$(echo $oldest_file | cut -d' ' -f1)
newest_time=$(echo $newest_file | cut -d' ' -f1)


# Convert times to seconds since the epoch, adjusting the time format
# Adjust for ISO 8601 format if needed
formatted_oldest_time="${oldest_time%.*}"
formatted_newest_time="${newest_time%.*}"
formatted_oldest_time=${formatted_oldest_time/+/'T'}
formatted_newest_time=${formatted_newest_time/+/'T'}

oldest_seconds=$(date -d "$formatted_oldest_time" +%s)
newest_seconds=$(date -d "$formatted_newest_time" +%s)

# Calculate the difference
time_diff=$((newest_seconds - oldest_seconds))

# Convert the difference to days, hours, minutes
days=$((time_diff / 86400))
hours=$(( (time_diff % 86400) / 3600))
minutes=$(( (time_diff % 3600) / 60))
seconds=$((time_diff % 60))

echo "The time difference between the oldest and newest file is $days days, $hours hours, $minutes minutes, $seconds seconds."

# Optional: Output the file names
echo "Oldest file: $(echo $oldest_file | cut -d' ' -f2-)"
echo "Newest file: $(echo $newest_file | cut -d' ' -f2-)"

