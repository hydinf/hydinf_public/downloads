# mHM-IMPORT by Thomas Kalbacher and Johannes Boog, UFZ, Leipzig, Germany
##########################################################################################
import sys
import os
from datetime import datetime
from vtk import *
from paraview.simple import *
import numpy as np
##########################################################################################
# files and parameter specification
#-----------------------------------------------------------------------------------------
# files and parameters
recharge_geotiff_path = str(sys.argv[1])
surface_vtu_path = str(sys.argv[2])
output_vtu_path = str(sys.argv[3])

file_path = os.getcwd()
print (file_path)

geotiff_file_name = file_path + '/' + recharge_geotiff_path
#geotiff_file_name = file_path + '/GIS_Recharge_Data/mHM/recharge_30years_dailymean__mm_per_day.tif'
vtu_input_filename = file_path + '/' + surface_vtu_path
#vtu_input_filename = file_path + '/OGS/Surface3D.vtu'
save_as_file_name = file_path + '/' + output_vtu_path 
#save_as_file_name = file_path + '/OGS/BC_Recharge_Steady.vtu'


print('START')
print('. . . Reading GEOTIFF')
# create a new 'GDAL Raster Reader'
recharge_input_tif = GDALRasterReader(registrationName='recharge_input.tif', FileName= geotiff_file_name)
# Properties modified on recharge_yearly_dailymean__mm_per_daytif
recharge_input_tif.CollateBands = 0

print('. . . Reading Extracted OGS Surface VTU')
# create a new 'XML Unstructured Grid Reader'
rechargeSurfacevtu = XMLUnstructuredGridReader(registrationName='RechargeSurface', FileName=[vtu_input_filename])
#rechargeSurfacevtu.CellArrayStatus = ['bulk_element_ids', 'bulk_face_ids']
#rechargeSurfacevtu.PointArrayStatus = ['bulk_node_ids', 'elevation']


# create a new 'Clean to Grid'
cleantoGrid1 = CleantoGrid(registrationName='CleantoGrid1', Input=recharge_input_tif)
cleantoGrid1.UpdatePipeline()
bounds = cleantoGrid1.GetDataInformation().GetBounds()
x_min_netcdf = bounds[0]
x_max_netcdf = bounds[1]
y_min_netcdf = bounds[2]
y_max_netcdf = bounds[3]

#Size of VTU
rechargeSurfacevtu.UpdatePipeline()
bounds = rechargeSurfacevtu.GetDataInformation().GetBounds()
x_min_vtu = bounds[0]
x_max_vtu = bounds[1]
y_min_vtu = bounds[2]
y_max_vtu = bounds[3]

print ("x_min_vtu = " + str(x_min_vtu) + " | x_min_netcdf = " + str(x_min_netcdf) )
print ("y_min_vtu = " + str(y_min_vtu) + " | y_min_netcdf = " + str(y_min_netcdf) )
print ("x_max_vtu = " + str(x_max_vtu) + " | x_max_netcdf = " + str(x_max_netcdf) )
print ("y_max_vtu = " + str(y_max_vtu) + " | x_max_netcdf = " + str(y_max_netcdf) )


# Clip Grid to DataSet with 
clip1 = Clip(registrationName='Clip1', Input=cleantoGrid1)
clip1.ClipType = 'Box'
clip1.ClipType.Position = [x_min_vtu, y_min_vtu, -10000.00]
clip1.ClipType.Length = [(x_max_vtu-x_min_vtu), (y_max_vtu-y_min_vtu), 20000.00]



# create a new 'Calculator - Elevation'
calculator0 = Calculator(registrationName='Calculator1', Input=rechargeSurfacevtu)
calculator0.ResultArrayName = 'elevation'
calculator0.Function = 'coordsZ'
UpdatePipeline(time=0.0, proxy=calculator0)


# create a new 'Transform'
transform1 = Transform(registrationName='Transform1', Input=calculator0)
# Properties modified on transform1.Transform
transform1.Transform.Scale = [1.0, 1.0, 0.0]


# create a new 'Resample With Dataset'
resampleWithDataset1 = ResampleWithDataset(registrationName='ResampleWithDataset1', SourceDataArrays=clip1, DestinationMesh=transform1)

# create a new 'Point Data to Cell Data'
pointDatatoCellData1 = PointDatatoCellData(registrationName='PointDatatoCellData1', Input=resampleWithDataset1)


# create a new 'Append Attributes'
appendAttributes1 = AppendAttributes(registrationName='AppendAttributes1', Input=[pointDatatoCellData1, transform1])


print ('Saving File: ' + str (save_as_file_name))
# save data
SaveData(save_as_file_name, proxy=appendAttributes1,DataMode='Ascii')



