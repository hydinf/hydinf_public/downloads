#Clipping by Thomas Kalbacher, UFZ, Leipzig, Germany
##########################################################################################
import sys
import os
from datetime import datetime
from vtk import *
from paraview.simple import *
import numpy as np
##########################################################################################
# parameter specification from input arguments
new_x_min_vtu = float (sys.argv[1])
new_x_max_vtu = float (sys.argv[2])
new_y_min_vtu = float (sys.argv[3])
new_y_max_vtu = float (sys.argv[4])
realtive_path = str(sys.argv[5])
#-----------------------------------------------------------------------------------------
# files and parameters
file_path = os.getcwd()

#-----------------------------------------------------------------------------------------
vtu_input_filename = file_path + realtive_path
#file_name_base = 'output_of_all_timesteps_0'
save_as_file_name = file_path + realtive_path
print (save_as_file_name)

print('. . . Reading OGS 2D Domain - VTU')
# create a new 'XML Unstructured Grid Reader'

#DomainVTU = vtkXMLPUnstructuredGridReader(registrationName='Domain', FileName=[vtu_input_filename])
DomainVTU = XMLUnstructuredGridReader(registrationName='Domain', FileName=[vtu_input_filename])
#rechargeSurfacevtu.CellArrayStatus = ['bulk_element_ids', 'bulk_face_ids']
#rechargeSurfacevtu.PointArrayStatus = ['bulk_node_ids']

#Size of VTU
DomainVTU.UpdatePipeline()
bounds = DomainVTU.GetDataInformation().GetBounds()
x_min_vtu = bounds[0]
x_max_vtu = bounds[1]
y_min_vtu = bounds[2]
y_max_vtu = bounds[3]


print ("x_min_vtu = " + str(x_min_vtu))
print ("x_max_vtu = " + str(x_max_vtu))
print ("y_min_vtu = " + str(y_min_vtu))
print ("y_max_vtu = " + str(y_max_vtu))

print ("Clipping domain along following box boundaries:")
print ("x_min_vtu = " + str(new_x_min_vtu))
print ("x_max_vtu = " + str(new_x_max_vtu))
print ("y_min_vtu = " + str(new_y_min_vtu))
print ("y_max_vtu = " + str(new_y_max_vtu))


# Clip Grid to DataSet with 
clip1 = Clip(registrationName='Clip1', Input=DomainVTU)
clip1.ClipType = 'Box'
clip1.Crinkleclip = 1
clip1.ClipType.Position = [new_x_min_vtu, new_y_min_vtu, -10000.00]
clip1.ClipType.Length = [(new_x_max_vtu-new_x_min_vtu), (new_y_max_vtu-new_y_min_vtu), 20000.00]

print ('Saving File: ' + str (save_as_file_name))
# save data
SaveData(save_as_file_name, proxy=clip1,DataMode='Ascii')



