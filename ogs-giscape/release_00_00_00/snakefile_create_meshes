configfile: "config/config.yaml"

##############################################################################
######### Meshes and Material Setup #####################################################
##############################################################################
# rules

rule SHP2GMSH_VTK_2DMeshing:
     input:
        "DataBase/Domains/{name}.shp",
        riverpoints=RIVER_POINTS_SHP_PATH_AND_FILE_NAME,
        monitoringpoints=MONITORING_POINTS_SHP_PATH_AND_FILE_NAME
     output:
        "DataSets/{name}/RiverLakes/Rivers.shp",
        "DataSets/{name}/Wells/Wells.shp",
        "DataSets/{name}/Hydrogeology/Domain2D.vtk"
     benchmark:
        "DataSets/{name}/SHP2GMSH_VTK_2DMeshing.bench"
     shell:
        "python3 Programs/scripts/python/py_seamsh_shp2gmsh_vtk.py {input[0]} {input.riverpoints} {input.monitoringpoints} {output[0]} {output[1]} {output[2]} 30 3000"

#old rule GMSH2DMeshing:
#     input:
#        "DataBase/Domains/{name}.geo"
#     output:
#        "DataBase/Domains/{name}.vtk"
#     shell:
#        "./Programs/bin/gmsh -2 {input} -algo delquad -epslc1d 1e-3 -format vtk -clmax 300 -clmin 30"


rule ConvertVTK2VTU_2DDomain:
     input:
        "DataSets/{name}/Hydrogeology/Domain2D.vtk"
     output:
        "DataSets/{name}/Hydrogeology/Domain2D.vtu"
     benchmark:
        "DataSets/{name}/ConvertVTK2VTU_2DDomain.bench"
     shell:
        "python3 Programs/scripts/python/py_vtk2vtu_unstructuredgrid.py {input} {output}"

rule Check2DMesh:
     input:
        "DataSets/{name}/Hydrogeology/Domain2D.vtu"
     output:
        "DataSets/{name}/Hydrogeology/Domain2D_CheckMeshProtocol.txt"
     benchmark:
        "DataSets/{name}/Check2DMesh.bench"
     shell:
        "apptainer exec ./Programs/bin/ogs_mpi.sif checkMesh {input} > {output}"


rule createLayeredMeshFromRasters:
     input:
        rules.Check2DMesh.output[0],
        "DataSets/{name}/Hydrogeology/Domain2D.vtu",
        "DataBase/Hydrogeology/BGR_Data/Germany_MAT_BGR.asc",
        "DataBase/Hydrogeology/DeutschlandDEM.asc",
        asclist=ASC_LAYER_ORDER_LIST_TXT,

     output:
        "DataSets/{name}/Hydrogeology/Subsurface3D.vtu",
        "DataSets/{name}/Hydrogeology/createLayeredMeshFromRastersProtocol.txt",
     benchmark:
        "DataSets/{name}/createLayeredMeshFromRasters.bench"
     shell:
        "./Programs/scripts/shell/createLayeredMeshFromRasters_control_script.sh {input} {output} {LAYERING_MODE}"


rule py_paraview_replaceMATbyNeighbor:
     input:
        "DataSets/{name}/Hydrogeology/createLayeredMeshFromRastersProtocol.txt"
     output:
        "DataSets/{name}/Hydrogeology/py_paraview_replaceMATbyNeighbor_protocol.txt"
     benchmark:
        "DataSets/{name}/py_paraview_replaceMATbyNeighbor.bench"
     shell:
        "python3 Programs/scripts/python/py_paraview_replaceMATbyNeighbor.py /DataSets/{wildcards.name}/Hydrogeology/Subsurface3D.vtu > {output}"


rule py_paraview_qualitybasedmaterials:
     input:
        "DataSets/{name}/Hydrogeology/py_paraview_replaceMATbyNeighbor_protocol.txt"
     output:
        "DataSets/{name}/Hydrogeology/py_paraview_qualitybasedmaterials_protocol.txt"
     benchmark:
        "DataSets/{name}/py_paraview_qualitybasedmaterials.bench"
     shell:
        "python3 Programs/scripts/python/py_paraview_qualitybasedmaterials.py /DataSets/{wildcards.name}/Hydrogeology/Subsurface3D.vtu > {output}"


rule NodeReordering:
     input:
        "DataSets/{name}/Hydrogeology/py_paraview_qualitybasedmaterials_protocol.txt",
     output:
        "DataSets/{name}/Hydrogeology/nodereordering_protocol.txt",
        "OGS_Setups/{name}/Subsurface3D.vtu"
     benchmark:
        "DataSets/{name}/NodeReordering.bench"
     shell:
        "apptainer exec ./Programs/bin/ogs_mpi.sif NodeReordering -i DataSets/{wildcards.name}/Hydrogeology/Subsurface3D.vtu -o {output[1]} > {output[0]}"

##############################################################################
######### Surface Generation for Boundary Conditions #########################
##############################################################################

rule ExtractSurface_for_BC_Setup:
     input:
        "OGS_Setups/{name}/Subsurface3D.vtu",
     output:
        temp("DataSets/{name}/Hydrogeology/Surface3D_without_elevation.vtu"),
     benchmark:
        "DataSets/{name}/ExtractSurface_for_BC_Setup.bench"
     container:
        "Programs/bin/ogs_mpi.sif"
     shell:
        "ExtractSurface -i {input} -o {output} -z -1 -a 65"

rule AddNodeAreaArrayToSurfaceMesh:
     input:
        "DataSets/{name}/Hydrogeology/Surface3D_without_elevation.vtu",
     output:
        temp("DataSets/{name}/Hydrogeology/Surface3D_with_node_area.vtu"),
     benchmark:
        "DataSets/{name}/AddNodeAreaArrayToSurfaceMesh.bench"
     shell:
        """
        python3 Programs/scripts/python/py_calculate_node_area.py {input} {output}
        """

rule AddElevationArrayToSurfaceMesh:
     input:
        "DataSets/{name}/Hydrogeology/Surface3D_with_node_area.vtu",
     output:
        "DataSets/{name}/Hydrogeology/Surface3D.vtu",
     benchmark:
        "DataSets/{name}/AddElevationArrayToSurfaceMesh.bench"
     shell:
        """
        python3 Programs/scripts/python/copyZCoordinatesToElevationArray.py {input} {output}
        python3 Programs/scripts/python/copyElevationArrayToZCoordinates.py DataSets/{wildcards.name}/Hydrogeology/Surface3D.vtu DataSets/{wildcards.name}/Hydrogeology/Surface3D.vtu
        """

