import sys
import os
from vtk import *
from paraview.simple import *
##########################################################################################
# files and parameters
#file_path = os.path.abspath(os.path.dirname(__file__))

realtive_path_domain = str(sys.argv[1])
file_path = os.getcwd()
#-----------------------------------------------------------------------------------------
vtu_river_file_name = file_path + realtive_path_domain
#vtu_river_file_name = file_path + '/OGS/BC_RiverLakes_Deutschland.vtu'

print ('Reading: ')    
print (vtu_river_file_name)


#########################################################################################
# create a new 'XML Unstructured Grid Reader'
rivervtu = XMLUnstructuredGridReader(registrationName='River.vtu', FileName=[vtu_river_file_name])
# Properties modified on rivervtu
rivervtu.PointArrayStatus = ['bulk_node_ids' , 'elevation']
rivervtu.TimeArray = 'None'
UpdatePipeline(time=0.0, proxy=rivervtu)

calculator_x1 = Calculator(registrationName='Calculator_X1', Input=rivervtu)
calculator_x1.ResultArrayName = 'X'
calculator_x1.Function = 'coordsX'
UpdatePipeline(time=0.0, proxy=calculator_x1)

calculator_y1 = Calculator(registrationName='Calculator_Y1', Input=calculator_x1)
calculator_y1.ResultArrayName = 'Y'
calculator_y1.Function = 'coordsY'
UpdatePipeline(time=0.0, proxy=calculator_y1)


# create a new 'Delaunay 2D'
delaunay2D2 = Delaunay2D(registrationName='Delaunay2D2', Input=calculator_y1)
delaunay2D2.ProjectionPlaneMode = 'Best-Fitting Plane'
UpdatePipeline(time=0.0, proxy=delaunay2D2)

# create a new 'Clean to Grid'
cleantoGrid0 = CleantoGrid(registrationName='CleantoGrid0', Input=delaunay2D2)
UpdatePipeline(time=0.0, proxy=cleantoGrid0)


# create a new 'Extract Edges'
extractEdges2 = ExtractEdges(registrationName='ExtractEdges2', Input=cleantoGrid0)
UpdatePipeline(time=0.0, proxy=extractEdges2)

# create a new 'Clean to Grid'
cleantoGrid1 = CleantoGrid(registrationName='CleantoGrid1', Input=extractEdges2)
UpdatePipeline(time=0.0, proxy=cleantoGrid1)

calculator_x2 = Calculator(registrationName='Calculator_X2', Input=cleantoGrid1)
calculator_x2.CoordinateResults = 1
calculator_x2.ResultArrayName = 'coordsX'
calculator_x2.Function = 'X*iHat + coordsY*jHat + coordsZ*kHat'
UpdatePipeline(time=0.0, proxy=calculator_x2)

calculator_y2 = Calculator(registrationName='Calculator_Y2', Input=calculator_x2)
calculator_y2.CoordinateResults = 1
calculator_y2.ResultArrayName = 'coordsY'
calculator_y2.Function = 'coordsX*iHat + Y*jHat + coordsZ*kHat'
UpdatePipeline(time=0.0, proxy=calculator_y2)


# create a new 'Cell Size'
cellSize2 = CellSize(registrationName='CellSize2', Input=calculator_y2)
cellSize2.ComputeVertexCount = 0
cellSize2.ComputeArea = 0
cellSize2.ComputeVolume = 0
UpdatePipeline(time=0.0, proxy=cellSize2)

# create a new 'Clean to Grid'
cleantoGrid2 = CleantoGrid(registrationName='CleantoGrid2', Input=cellSize2)
UpdatePipeline(time=0.0, proxy=cleantoGrid2)


# create a new 'Threshold'
threshold2 = Threshold(registrationName='Threshold2', Input=cellSize2)
threshold2.Scalars = ['CELLS', 'Length']
threshold2.LowerThreshold = 0.0
threshold2.UpperThreshold = 111.0
UpdatePipeline(time=0.0, proxy=threshold2)


# create a new 'Calculator'
calculator2 = Calculator(registrationName='Calculator3', Input=cleantoGrid2)
calculator2.ResultArrayName = 'waterelevation'
calculator2.Function = 'elevation'
UpdatePipeline(time=0.0, proxy=calculator2)




# create a new 'Calculator'
calculator3 = Calculator(registrationName='Calculator3', Input=calculator2)
calculator3.ResultArrayName = 'bulk_node_ids'
calculator3.Function = 'bulk_node_ids'
calculator3.ResultArrayType = 'Unsigned Long'
UpdatePipeline(time=0.0, proxy=calculator3)

# create a new 'PassArrays'
passArrays2 = PassArrays(registrationName='PassArrays2',Input=calculator3)
passArrays2.PointDataArrays = ['bulk_node_ids', 'elevation', 'waterelevation']
UpdatePipeline(time=0.0, proxy=passArrays2)



# create a new 'Threshold'
threshold1 = Threshold(registrationName='Threshold1', Input=passArrays2)
threshold1.Scalars = ['POINTS', 'elevation']
threshold1.LowerThreshold = 50.0
threshold1.UpperThreshold = 1500.0




# save data
SaveData(vtu_river_file_name, proxy=threshold1, DataMode='Binary')
#print (vtu_file_name_out)



