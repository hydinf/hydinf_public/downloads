#!/usr/bin/env bash

wd=`pwd`
echo "working directory: ${wd}"
vtkdiff="apptainer exec ${wd}/Programs/bin/ogs_mpi.sif vtkdiff"
echo "vtkdiff command: ${vtkdiff}"

path=OGS_Results/Abens_Debug01/SteadyState/02/
declare -a variables=('VolumetricFlowRate' 'head')

function vtkcompare()
{
    files=`ls $1_*.vtu`
    if (( ${#files[@]} == 0 )); then
        exit 2
    fi
    for f in `ls $1_*.vtu`;
    do
        ${vtkdiff} -a $2 -b $2 ${f} ${wd}/.tests/end2end/${path}/${f}
        comparison_result=$?
        if test ${comparison_result} -eq 1
        then
            exit 2
        fi
    done
}

cd ${path}
for variable in "${variables[@]}";
do
    vtkcompare MOD_01_Subsurface3D_ts_10_t_315360000000_000000 $variable
    vtkcompare MOD_01_OUT_Recharge_ts_10_t_315360000000_000000 $variable
    vtkcompare MOD_01_OUT_Rivers_ts_10_t_315360000000_000000 $variable
done
cd ${wd}
exit 0

