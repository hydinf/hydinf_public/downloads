import sys
import os
from vtk import vtkPolyDataReader, vtkXMLPolyDataWriter, vtkIdList

def construct_file_path(relative_path):
    """
    Constructs a full file path given a relative path.

    Parameters:
    relative_path (str): The relative file path.

    Returns:
    str: The full file path combining the current working directory with the relative path.
    """
    return os.path.join(os.getcwd(), relative_path)

def read_vtu_file(file_path):
    """
    Reads a VTU file using a VTK PolyDataReader.

    Parameters:
    file_path (str): The path to the VTU file to read.

    Returns:
    vtkPolyData: The mesh data from the VTU file.
    """
    reader = vtkPolyDataReader()
    reader.SetFileName(file_path)
    reader.Update()
    return reader.GetOutput()

def write_vtu_file(mesh, output_file_path):
    """
    Writes a VTU file using a VTK XML PolyDataWriter in binary mode.

    Parameters:
    mesh (vtkPolyData): The mesh data to write.
    output_file_path (str): The path to the output VTU file.
    """
    writer = vtkXMLPolyDataWriter()
    writer.SetFileName(output_file_path)
    writer.SetDataModeToBinary()
    writer.SetInputData(mesh)
    writer.Write()

def main(input_file, output_file):
    """
    Main function to convert a VTU file from ASCII to Binary format.

    Parameters:
    input_file (str): Relative path to the input VTU file.
    output_file (str): Relative path to the output VTU file.
    """
    print('Converting ASCII into BINARY:')
    input_path = construct_file_path(input_file)
    print(input_path)
    mesh = read_vtu_file(input_path)
    output_path = construct_file_path(output_file)
    write_vtu_file(mesh, output_path)

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("Usage: python script.py <inputfile> <outputfile>")
        sys.exit(1)
    main(sys.argv[1], sys.argv[2])

