import sys
import os
import csv

def create_tpl_file():
    # files and parameters
    relative_input_file_path = str(sys.argv[1])
    relative_output_file_path = str(sys.argv[2])
    #file_path = os.path.abspath(os.path.dirname(__file__))
    file_path = os.getcwd()
    #print(file_path)
    csv_file_name = file_path + "/" + relative_input_file_path
    tpl_file_name_out = file_path + "/" + relative_output_file_path

    print('Creating TPL File for PEST based on:')
    print(csv_file_name)

    # Open the input CSV file for reading and the output TPL file for writing
    with open(csv_file_name, 'r') as input_file, open(tpl_file_name_out, 'w', newline='') as output_file:
        # Create a CSV reader and writer objects
        reader = csv.reader(input_file)
        writer = csv.writer(output_file)
        counter = 0
        writer.writerow(['ptf !'])
        for row in reader:
            if counter == 0:
                writer.writerow(row)
            if counter > 0:
                row[2] = f"!k{counter-1}           !"  # modify the third column by adding the template tag
                writer.writerow(row)
            counter += 1

def main():
    create_tpl_file()

if __name__ == "__main__":
    main()
