import sys
import os
from vtk import *
from paraview.simple import *
##########################################################################################
# files and parameters
#file_path = os.path.abspath(os.path.dirname(__file__))
realtive_input_file_path = str(sys.argv[1])
file_path = os.getcwd()
#print (file_path)
vtu_river_file_name = file_path + realtive_input_file_path
vtu_river_file_name_out = vtu_river_file_name.replace('.vtp', '.vtu')

print ('Reading: ')    
print (vtu_river_file_name)

rivervtu = XMLPolyDataReader(registrationName='River.vtu', FileName=[vtu_river_file_name])
rivervtu.PointArrayStatus = ['bulk_node_ids' , 'elevation', 'waterelevation']
rivervtu.TimeArray = 'None'
UpdatePipeline(time=0.0, proxy=rivervtu)

# create a new 'Convert To Point Cloud'
convertToPointCloud2 = ConvertToPointCloud(registrationName='ConvertToPointCloud2', Input=rivervtu)
convertToPointCloud2.CellGenerationMode = 'Vertex cells'
UpdatePipeline(time=0.0, proxy=convertToPointCloud2)

# create a new 'Clean to Grid'
cleantoGrid2 = CleantoGrid(registrationName='CleantoGrid2', Input=convertToPointCloud2)
UpdatePipeline(time=0.0, proxy=cleantoGrid2)


# save data
print ('Writing: ')    
print (vtu_river_file_name_out)
SaveData(vtu_river_file_name_out, proxy=cleantoGrid2, DataMode='Binary')
#print (vtu_file_name_out)


