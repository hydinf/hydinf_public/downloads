subfolder_name=${PWD##*/}          # to assign to a variable
subfolder_name=${subfolder_name:-/}        # to correct for the case where PWD=/


#!/usr/bin/env bash
# values to change
MESH_BASENAME=Subsurface3D
MESH=${MESH_BASENAME}.vtu
BC_MESHES='BC_RiverLakes_Deutschland.vtu Surface3D.vtu OUT_Recharge.vtu OUT_Rivers.vtu'
# end values to change

container_path=../../../Programs/bin/ogs_mpi.sif
# Identify Subdomains to ensure matching node and element IDs
apptainer exec ${container_path} identifySubdomains -f -m Subsurface3D.vtu -s 0.1 BC_RiverLakes_Deutschland.vtu
apptainer exec ${container_path} identifySubdomains -f -m Subsurface3D.vtu -s 0.1 OUT_Recharge.vtu
apptainer exec ${container_path} identifySubdomains -f -m Subsurface3D.vtu -s 0.1 OUT_Rivers.vtu
apptainer exec ${container_path} identifySubdomains -f -m Subsurface3D.vtu -s 0.1 Surface3D.vtu


number_of_partitions=$1
model_domain_name=$2

if [ ! -d ${number_of_partitions} ];
then
    mkdir ${number_of_partitions}
fi

if [ ! -e ${MESH_BASENAME}.mesh ];
then
    apptainer exec ${container_path} partmesh --ogs2metis -i ${MESH}
fi

if [ ! -e ${number_of_partitions}/${MESH_BASENAME}.mesh ];
then
    cd ${number_of_partitions}
    #ln -s ../${MESH_BASENAME}.mesh ${MESH_BASENAME}.mesh
    cp ../${MESH_BASENAME}.mesh ${MESH_BASENAME}.mesh
    cd ..
fi

apptainer exec ${container_path} partmesh -m -n ${number_of_partitions} -i ${MESH} -o ${number_of_partitions} -- ${BC_MESHES}

echo "#############################################################"
echo "# Steady state HPC - Parallel OpenMPI Run: $subfolder_name  "
echo "#############################################################"

declare -a project_files=(
    '000_ModelRun.prj'
    '000_Media.xml'
    '000_TimeStepping.xml'
    '000_RasterRechargeParameterTimeSeries.xml'
    '000_RasterRechargeParameters.xml'
    '000_RasterRechargeXMLFile.xml'
    'BC_Recharge_Steady.nc'
)

mkdir -p ../../../OGS_Results/${model_domain_name}/SteadyState/${number_of_partitions}

cd ${number_of_partitions}
# create links for project files
for project_file in "${project_files[@]}";
do
    ln -s ../${project_file} ${project_file}
done

apptainer exec ../${container_path} mpirun -np ${number_of_partitions} ogs -o ../../../../OGS_Results/${model_domain_name}/SteadyState/${number_of_partitions} 000_ModelRun.prj
