# Paraview-Time-Step and Block Merging  by Thomas Kalbacher, UFZ, Leipzig, Germany
##########################################################################################
import sys
import os
import shutil
from datetime import datetime
from vtk import *
from paraview.simple import *
import glob, os

filenameslist = []

print("Looping over Name list")

os.chdir("GMSH/Output")
for file in glob.glob("*.vtu"):
    filenameslist.append(file) 

a2D_DomainGeometry_ = XMLUnstructuredGridReader(FileName=filenameslist)

print("Grouping Time Steps")

# create a new 'Group Time Steps'
groupTimeSteps1 = GroupTimeSteps(Input=a2D_DomainGeometry_)

print("Merging Blocks")
# create a new 'Merge Blocks'
mergeBlocks1 = MergeBlocks(Input=groupTimeSteps1)


# create a new 'XML Unstructured Grid Reader'
RiversPoints4Flux = XMLUnstructuredGridReader(registrationName='RiversPoints4Flux', FileName=['../../GIS_Rivers_Data/RiversPoints4Flux.vtu'])
pointDatasetInterpolator1 = PointDatasetInterpolator(registrationName='PointDatasetInterpolator1', Input=RiversPoints4Flux, Source=mergeBlocks1)
pointDatasetInterpolator1.Kernel = 'LinearKernel'
pointDatasetInterpolator1.Kernel.KernelFootprint = 'N Closest'
pointDatasetInterpolator1.Kernel.NumberOfPoints = 1


print("Convert to point cloud")

# create a new 'Convert To Point Cloud'
convertToPointCloud1 = ConvertToPointCloud(registrationName='ConvertToPointCloud1', Input=pointDatasetInterpolator1)

# Properties modified on convertToPointCloud1
convertToPointCloud1.CellGenerationMode = 'Vertex cells'
cleantoGrid1 = CleantoGrid(registrationName='CleantoGrid1', Input=convertToPointCloud1)
# create a new 'Calculator'
calculator1 = Calculator(registrationName='Calculator1', Input=cleantoGrid1)
# Properties modified on calculator1
calculator1.ResultArrayName = 'bulk_node_ids'
calculator1.Function = 'bulk_node_ids'
calculator1.ResultArrayType = 'Unsigned Long'

print("Save point cloud")

# save data
SaveData('../../GIS_Rivers_Data/RiversPoints_OriginFile.vtu', proxy=mergeBlocks1)
#shutil.rmtree('../Output')

print("Saving done")

