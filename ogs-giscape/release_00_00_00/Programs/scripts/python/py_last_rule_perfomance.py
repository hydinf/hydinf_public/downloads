import sys
import os
import csv
from datetime import datetime
import pandas as pd
import matplotlib.pyplot as plt
import glob

def get_benchmarks_dir():
    """
    Returns the directory containing benchmark files.
    """
    return os.path.dirname(os.path.realpath(__file__))

def collect_benchmark_data(benchmarks_dir):
    """
    Collects benchmark data from .bench files within a specified directory.

    Parameters:
    benchmarks_dir (str): Path to the directory containing the .bench files.

    Returns:
    list: A list of dictionaries, each containing data from one benchmark file.
    """
    output_csv_path = os.path.join(benchmarks_dir, 'snakemake_rule_performance.csv')
    data = []
    fieldnames = ['rule_name', 'file_modification_time', 's', 'h:m:s', 'max_rss', 'max_vms', 'max_uss', 'max_pss', 'io_in', 'io_out', 'mean_load', 'cpu_time']

    for bench_file in glob.iglob(os.path.join(benchmarks_dir, '**', '*.bench'), recursive=True):
        rule_name = os.path.basename(os.path.dirname(bench_file)) + "_" + os.path.basename(bench_file).replace('.bench', '')
        mod_time = datetime.fromtimestamp(os.path.getmtime(bench_file)).strftime('%Y-%m-%d %H:%M:%S')

        with open(bench_file, 'r') as file:
            next(file)  # Skip header
            reader = csv.reader(file, delimiter='\t')
            for row in reader:
                row_data = {fieldnames[i]: value for i, value in enumerate([rule_name, mod_time] + row)}
                data.append(row_data)

    with open(output_csv_path, 'w', newline='') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        for row in data:
            writer.writerow(row)

    print(f"Summary of benchmarks has been saved to {output_csv_path}.")
    return output_csv_path

def analyze_and_plot_benchmarks(csv_file_path, benchmarks_dir):
    """
    Analyzes benchmark data and plots the results.

    Parameters:
    csv_file_path (str): The path to the CSV file containing benchmark data.
    benchmarks_dir (str): The directory to save the plots.
    """
    df = pd.read_csv(csv_file_path)
    df['s'] = pd.to_numeric(df['s'], errors='coerce')
    df['max_rss'] = pd.to_numeric(df['max_rss'], errors='coerce')

    total_time = df['s'].sum()
    total_memory = df['max_rss'].sum()
    df['time_percentage'] = (df['s'] / total_time) * 100
    df['memory_percentage'] = (df['max_rss'] / total_memory) * 100

    total_time_by_rule = df.groupby('rule_name')['s'].sum()
    total_memory_by_rule = df.groupby('rule_name')['max_rss'].sum()

    plot_data(total_time_by_rule, 'Total Time in Seconds', 'Absolute Total Runtime for Each Rule', 'absolute_time_usage_plot.png', benchmarks_dir)
    plot_data(total_memory_by_rule, 'Total Memory Usage in MB', 'Absolute Total Memory Usage for Each Rule', 'absolute_memory_usage_plot.png', benchmarks_dir)

def plot_data(data_series, xlabel, title, filename, output_dir):
    """
    Plots a horizontal bar graph of the data series.

    Parameters:
    data_series (pd.Series): Data to plot.
    xlabel (str): Label for the x-axis.
    title (str): Title of the plot.
    filename (str): Filename for the saved plot.
    output_dir (str): Directory to save the plot.
    """
    data_series.sort_values().plot(kind='barh', figsize=(10, 8), color='coral')
    plt.xlabel(xlabel)
    plt.ylabel('Rule')
    plt.title(title)
    plt.tight_layout()
    plt.savefig(os.path.join(output_dir, filename))
    plt.clf()

def main():
    """
    Main function to execute the workflow for collecting, analyzing, and plotting benchmark data.
    """
    if len(sys.argv) < 1:
        print("Usage: python script.py")
        sys.exit(1)

    benchmarks_dir = get_benchmarks_dir()
    csv_file_path = collect_benchmark_data(benchmarks_dir)
    analyze_and_plot_benchmarks(csv_file_path, benchmarks_dir)

if __name__ == "__main__":
    main()

