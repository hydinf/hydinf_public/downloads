echo "#############################################################"
echo "Start OpenGeoSys Simulation for a STEADY-State Simulation"
echo "#############################################################"
python3 py_create_OGSmaterialXMLfromCSV.py MaterialProperties.csv 2 000_Media.xml
singularity exec ogs.sif ogs -o ResultsSteadyState 000_ModelRun.prj
pvpython py_pv_vtu2csv.py ResultsSteadyState/MOD_01_MonitoringWells_ts_100_t_3153600000000.000000.vtu ResultsSteadyState/Simulated_Head_AllMonitoringWells_SteadyState.csv
cp ResultsSteadyState/MOD_01_Subsurface3D_ts_100_t_3153600000000.000000.vtu Subsurface3D_IC.vtu
singularity exec ogs.sif ogs -o ResultsTransientState 001_ModelRun.prj
pvpython py_pv_pvd2csv.py ResultsTransientState/MOD_01_MonitoringWells.pvd ResultsTransientState/Head_AllMonitoringWells_at_timestep.csv
echo "#############################################################"
echo "END OF SCRIPT"
echo "#############################################################"







