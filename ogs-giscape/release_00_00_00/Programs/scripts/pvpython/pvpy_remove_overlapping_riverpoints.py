#Clipping by Thomas Kalbacher, UFZ, Leipzig, Germany
##########################################################################################
import sys
import os
from datetime import datetime
from vtk import *
from paraview.simple import *
import numpy as np
##########################################################################################
# parameter specification from input arguments
realtive_path_domain = str(sys.argv[1])
realtive_path_river = str(sys.argv[2])

#skalar_type = str(sys.argv[2])
#skalar_name = str(sys.argv[3])
#LowerThreshold = float(sys.argv[4])
#UpperThreshold = float(sys.argv[5])


#skalar_type = 'POINTS'
#skalar_name = 'elevation'
#LowerThreshold = 0.0001
#UpperThreshold = 1000.0


#-----------------------------------------------------------------------------------------
# files and parameters
file_path = os.getcwd()
#-----------------------------------------------------------------------------------------
vtu_input_filename = file_path + realtive_path_domain

save_as_file_name = file_path + realtive_path_river



print('. . . Reading: ' +  str(save_as_file_name))
# create a new 'XML Unstructured Grid Reader'
DomainVTU = XMLUnstructuredGridReader(registrationName='Domain', FileName=[vtu_input_filename])
RiverVTU = XMLUnstructuredGridReader(registrationName='River', FileName=[save_as_file_name])

#rechargeSurfacevtu.CellArrayStatus = ['bulk_element_ids', 'bulk_face_ids']
#rechargeSurfacevtu.PointArrayStatus = ['bulk_node_ids']
DomainVTU.UpdatePipeline()
RiverVTU.UpdatePipeline()



# create a new 'Calculator'
#calculator1 = Calculator(registrationName='Calculator1', Input=DomainVTU)
#calculator1.ResultArrayName = 'InsideDomain'
#calculator1.Function = '1'
#UpdatePipeline(time=0.0, proxy=calculator1)



# create a new 'Calculator'
DomainVTU_2D = Calculator(registrationName='DomainVTU_2D', Input=DomainVTU)
DomainVTU_2D.CoordinateResults = 1
DomainVTU_2D.ResultArrayName = 'coordsZ'
DomainVTU_2D.Function = 'coordsX*iHat + coordsY*jHat + (0.0) *kHat'
UpdatePipeline(time=0.0, proxy=DomainVTU_2D)

# create a new 'Calculator'
RiverVTU_2D = Calculator(registrationName='RiverVTU_2D', Input=RiverVTU)
RiverVTU_2D.CoordinateResults = 1
RiverVTU_2D.ResultArrayName = 'coordsZ'
RiverVTU_2D.Function = 'coordsX*iHat + coordsY*jHat + (0.0) *kHat'
UpdatePipeline(time=0.0, proxy=RiverVTU_2D)




# create a new 'Point Dataset Interpolator'
pointDatasetInterpolator1 = PointDatasetInterpolator(registrationName='PointDatasetInterpolator1', Input=DomainVTU_2D, Source=RiverVTU_2D)
pointDatasetInterpolator1.Kernel = 'GaussianKernel'
pointDatasetInterpolator1.Kernel.Radius = 0
pointDatasetInterpolator1.Kernel.KernelFootprint = 'Radius'

#pointDatasetInterpolator1.Locator = 'Static Point Locator'
#pointDatasetInterpolator1.Kernel = 'LinearKernel'
#pointDatasetInterpolator1.Kernel.Radius = 0.5

UpdatePipeline(time=0.0, proxy=pointDatasetInterpolator1)





# create a new 'Threshold'
threshold1 = Threshold(registrationName='Threshold1', Input=pointDatasetInterpolator1)
#threshold1.LowerThreshold = 1.0
threshold1.Scalars = ['POINTS', 'bulk_node_ids']
threshold1.LowerThreshold = 1.0
threshold1.UpperThreshold = 1000000000.0
UpdatePipeline(time=0.0, proxy=threshold1)


# create a new 'Clean to Grid'
cleantoGrid2 = CleantoGrid(registrationName='CleantoGrid2', Input=threshold1)
UpdatePipeline(time=0.0, proxy=cleantoGrid2)

# create a new 'Calculator'
calculator3 = Calculator(registrationName='Calculator3', Input=cleantoGrid2)
# Properties modified on calculator1
calculator3.ResultArrayName = 'bulk_node_ids'
calculator3.Function = 'bulk_node_ids'
calculator3.ResultArrayType = 'Unsigned Long'
UpdatePipeline(time=0.0, proxy=calculator3)

# create a new 'PassArrays'
passArrays2 = PassArrays(registrationName='PassArrays2',Input=calculator3)
passArrays2.PointDataArrays = ['bulk_node_ids', 'elevation']
UpdatePipeline(time=0.0, proxy=passArrays2)



print ('Saving File: ' + str (save_as_file_name))
# save data
SaveData(save_as_file_name, proxy=passArrays2,DataMode='Binary')



