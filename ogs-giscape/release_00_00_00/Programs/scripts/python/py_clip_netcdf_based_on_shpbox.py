import os
import sys
import geopandas as gpd
import rasterio
from rasterio.mask import mask

def get_full_path(relative_path):
    """
    Constructs the full path for a file based on the current working directory and a given relative path.

    Parameters:
    relative_path (str): The relative path to the file.

    Returns:
    str: The absolute path to the file.
    """
    return os.path.join(os.getcwd(), relative_path)

def clip_raster_with_shapefile(shapefile_path, raster_path, output_raster_path):
    """
    Clips a raster file using the boundaries of a shapefile and saves the clipped raster.

    Parameters:
    shapefile_path (str): Path to the shapefile used as a mask.
    raster_path (str): Path to the raster file to be clipped.
    output_raster_path (str): Path where the clipped raster will be saved.
    """
    # Load the bounding shapefile
    bounding_shapefile = gpd.read_file(shapefile_path)

    # Open the raster file
    with rasterio.open(raster_path) as src:
        # Ensure the shapefile's CRS matches the raster's CRS
        if bounding_shapefile.crs != src.crs:
            bounding_shapefile = bounding_shapefile.to_crs(src.crs)

        # Convert the GeoDataFrame to a GeoJSON-like dictionary
        shapes = [feature["geometry"] for _, feature in bounding_shapefile.iterrows()]

        # Clip the raster using rasterio's mask function with all_touched=True
        out_image, out_transform = mask(src, shapes, crop=True, all_touched=True)
        out_meta = src.meta.copy()

    # Update the metadata for the clipped raster
    out_meta.update({
        "driver": "GTiff",
        "height": out_image.shape[1],
        "width": out_image.shape[2],
        "transform": out_transform
    })

    # Save the clipped raster
    with rasterio.open(output_raster_path, "w", **out_meta) as dest:
        dest.write(out_image)

    print(f"Clipped raster file saved as {output_raster_path}")

def main():
    """
    Main function to execute the raster clipping process based on command-line arguments.
    """
    if len(sys.argv) < 4:
        print("Usage: python script.py <bounding_shapefile> <raster_file> <output_clipped_raster>")
        sys.exit(1)

    shapefile_path = get_full_path(sys.argv[1])
    raster_path = get_full_path(sys.argv[2])
    output_raster_path = get_full_path(sys.argv[3])

    clip_raster_with_shapefile(shapefile_path, raster_path, output_raster_path)

if __name__ == "__main__":
    main()

