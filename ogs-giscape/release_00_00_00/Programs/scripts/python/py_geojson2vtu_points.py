import sys
import os
import vtk
import json
import numpy as np

def get_full_path(relative_path):
    """
    Constructs the full path for a file based on the current working directory and a given relative path.

    Parameters:
    relative_path (str): The relative path to the file.

    Returns:
    str: The absolute path to the file.
    """
    return os.path.join(os.getcwd(), relative_path)

def load_geojson(file_path):
    """
    Loads a GeoJSON file and returns the data.

    Parameters:
    file_path (str): The path to the GeoJSON file.

    Returns:
    dict: The GeoJSON data loaded from the file.
    """
    with open(file_path) as f:
        return json.load(f)

def create_unstructured_grid(geojson_data):
    """
    Creates a VTK unstructured grid from GeoJSON point data.

    Parameters:
    geojson_data (dict): GeoJSON data containing point features.

    Returns:
    vtk.vtkUnstructuredGrid: The constructed unstructured grid.
    """
    ugrid = vtk.vtkUnstructuredGrid()
    points = vtk.vtkPoints()
    points.SetDataTypeToDouble()

    for feature in geojson_data['features']:
        geometry = feature['geometry']
        if geometry['type'] == 'Point':
            x, y = geometry['coordinates']
            points.InsertNextPoint(float(x), float(y), float(0.0))
    ugrid.SetPoints(points)

    cell_array = vtk.vtkCellArray()
    for i in range(points.GetNumberOfPoints()):
        cell = vtk.vtkVertex()
        cell.GetPointIds().SetId(0, i)
        cell_array.InsertNextCell(cell)

    ugrid.SetCells(vtk.VTK_VERTEX, cell_array)
    return ugrid

def write_unstructured_grid_to_vtu(ugrid, output_file_path):
    """
    Writes a VTK unstructured grid to a VTU file.

    Parameters:
    ugrid (vtk.vtkUnstructuredGrid): The unstructured grid to write.
    output_file_path (str): The path to the output VTU file.
    """
    writer = vtk.vtkXMLUnstructuredGridWriter()
    writer.SetFileName(output_file_path)
    writer.SetInputData(ugrid)
    writer.Write()

def main():
    """
    Main function to execute the conversion of GEOJSON points to a VTU file.
    """
    if len(sys.argv) < 3:
        print("Usage: python script.py <input_geojson_file> <output_vtu_file>")
        sys.exit(1)

    geojson_file_path = get_full_path(sys.argv[1])
    vtu_file_path_out = get_full_path(sys.argv[2])

    print('Converting GEOJSON points to VTU:')
    print(geojson_file_path)

    geojson_data = load_geojson(geojson_file_path)
    ugrid = create_unstructured_grid(geojson_data)
    write_unstructured_grid_to_vtu(ugrid, vtu_file_path_out)

if __name__ == "__main__":
    main()

