rm -rf ./DataSets
mkdir ./DataSets

rm -rf ./OGS_Setups
mkdir ./OGS_Setups

rm -rf ./OGS_Results
mkdir ./OGS_Results

rm -rf .snakemake/
rm -rf ./ci
rm -rf ./docs
rm -rf ./profiles
rm -rf ./.tests
rm -rf ./.git
rm -rf pyproject.toml
rm -rf README.md
rm -rf .gitignore
rm -rf .gitlab-ci.yml
