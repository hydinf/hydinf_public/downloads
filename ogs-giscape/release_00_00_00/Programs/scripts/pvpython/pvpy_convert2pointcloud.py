import sys
import os
from vtk import *
from paraview.simple import *
##########################################################################################
# files and parameters
#file_path = os.path.abspath(os.path.dirname(__file__))
realtive_path_domain = str(sys.argv[1])
file_path = os.getcwd()
#print (file_path)
vtu_river_file_name = file_path + realtive_path_domain
vtu_river_file_name_out = vtu_river_file_name


print ('Reading: ')    
print (vtu_river_file_name)


rivervtu = XMLUnstructuredGridReader(registrationName='River.vtu', FileName=[vtu_river_file_name])
rivervtu.PointArrayStatus = ['bulk_node_ids' , 'elevation', 'waterelevation']
rivervtu.TimeArray = 'None'
UpdatePipeline(time=0.0, proxy=rivervtu)

# create a new 'Convert To Point Cloud'
convertToPointCloud2 = ConvertToPointCloud(registrationName='ConvertToPointCloud2', Input=rivervtu)
convertToPointCloud2.CellGenerationMode = 'Vertex cells'
UpdatePipeline(time=0.0, proxy=convertToPointCloud2)

# create a new 'Clean to Grid'
cleantoGrid2 = CleantoGrid(registrationName='CleantoGrid2', Input=convertToPointCloud2)
UpdatePipeline(time=0.0, proxy=cleantoGrid2)

# create a new 'Calculator'
calculator3 = Calculator(registrationName='Calculator3', Input=cleantoGrid2)
# Properties modified on calculator1
calculator3.ResultArrayName = 'bulk_node_ids'
calculator3.Function = 'bulk_node_ids'
calculator3.ResultArrayType = 'Unsigned Long'
UpdatePipeline(time=0.0, proxy=calculator3)

# create a new 'PassArrays'
passArrays2 = PassArrays(registrationName='PassArrays2',Input=calculator3)
passArrays2.PointDataArrays = ['bulk_node_ids', 'elevation', 'waterelevation']
UpdatePipeline(time=0.0, proxy=passArrays2)



# save data
print ('Writing: ')    
print (vtu_river_file_name_out)
SaveData(vtu_river_file_name_out, proxy=passArrays2, DataMode='Binary')
#print (vtu_file_name_out)


