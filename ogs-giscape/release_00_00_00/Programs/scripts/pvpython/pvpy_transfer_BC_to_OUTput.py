import sys
import os
from vtk import *
from paraview.simple import *
##########################################################################################
# files and parameters
# parameter specification from input arguments
realtive_project_path = str(sys.argv[1])
file_path = os.getcwd()
#print (file_path)
vtu_file_name = file_path + realtive_project_path + '/Surface3D.vtu'
vtu_file_name_out = file_path + realtive_project_path + '/OUT_Recharge.vtu'
vtu_river_file_name = file_path + realtive_project_path + '/BC_RiverLakes_Deutschland.vtu'
vtu_river_file_name_out = file_path + realtive_project_path + '/OUT_Rivers.vtu'

print ('Reading: ')    
print (vtu_file_name)

##########################################################################################
# create a new 'XML Unstructured Grid Reader'
rechargevtu = XMLUnstructuredGridReader(registrationName='Recharge.vtu', FileName=[vtu_file_name])
rivervtu = XMLUnstructuredGridReader(registrationName='River.vtu', FileName=[vtu_river_file_name])

# Properties modified on rechargevtu
rechargevtu.CellArrayStatus = []
rechargevtu.PointArrayStatus = ['bulk_node_ids', 'elevation', 'NodeArea']
rechargevtu.TimeArray = 'None'
# Properties modified on rivervtu
rivervtu.PointArrayStatus = ['bulk_node_ids' , 'elevation', 'NodeArea']
rivervtu.TimeArray = 'None'

UpdatePipeline(time=0.0, proxy=rivervtu)


# create a new 'Calculator - River'
calculator1 = Calculator(registrationName='Calculator1', Input=rivervtu)
calculator1.ResultArrayName = 'River'
calculator1.Function = '1'
UpdatePipeline(time=0.0, proxy=calculator1)
# create a new 'Point Dataset Interpolator'
pointDatasetInterpolator1 = PointDatasetInterpolator(registrationName='PointDatasetInterpolator1', Input=calculator1, Source=rechargevtu)
# Properties modified on pointDatasetInterpolator1
pointDatasetInterpolator1.Kernel = 'GaussianKernel'
pointDatasetInterpolator1.Kernel.KernelFootprint = 'N Closest'
UpdatePipeline(time=0.0, proxy=pointDatasetInterpolator1)

# create a new 'Calculator'
calculator2 = Calculator(registrationName='Calculator2', Input=pointDatasetInterpolator1)


###Recharge###
# create a new 'Threshold'
threshold1 = Threshold(registrationName='Threshold1', Input=pointDatasetInterpolator1)
threshold1.UpperThreshold = 0.0
UpdatePipeline(time=0.0, proxy=threshold1)

# create a new 'Resample With Dataset'
resampleWithDataset1 = ResampleWithDataset(registrationName='ResampleWithDataset1', SourceDataArrays=rechargevtu,  DestinationMesh=threshold1)
#resampleWithDataset1.PassFieldArrays = 0
UpdatePipeline(time=0.0, proxy=resampleWithDataset1)

# create a new 'Convert To Point Cloud'
convertToPointCloud1 = ConvertToPointCloud(registrationName='ConvertToPointCloud1', Input=resampleWithDataset1)
convertToPointCloud1.CellGenerationMode = 'Vertex cells'
UpdatePipeline(time=0.0, proxy=convertToPointCloud1)

# create a new 'Clean to Grid'
cleantoGrid1 = CleantoGrid(registrationName='CleantoGrid1', Input=convertToPointCloud1)
# Properties modified on oUT_Rechargevtu
UpdatePipeline(time=0.0, proxy=cleantoGrid1)

# create a new 'PassArrays'
passArrays1 = PassArrays(registrationName='PassArrays1', Input=cleantoGrid1)
passArrays1.PointDataArrays = ['bulk_node_ids', 'elevation', 'NodeArea']
UpdatePipeline(time=0.0, proxy=passArrays1)

# save data
SaveData(vtu_file_name_out, proxy=passArrays1, DataMode='Binary')
#print (vtu_file_name_out)

###Rivers###

# create a new 'Convert To Point Cloud'
convertToPointCloud2 = ConvertToPointCloud(registrationName='ConvertToPointCloud2', Input=calculator1)
convertToPointCloud2.CellGenerationMode = 'Vertex cells'
UpdatePipeline(time=0.0, proxy=convertToPointCloud2)

# create a new 'Clean to Grid'
cleantoGrid2 = CleantoGrid(registrationName='CleantoGrid2', Input=convertToPointCloud2)
UpdatePipeline(time=0.0, proxy=cleantoGrid2)

# create a new 'Calculator'
calculator3 = Calculator(registrationName='Calculator3', Input=cleantoGrid2)
# Properties modified on calculator1
calculator3.ResultArrayName = 'bulk_node_ids'
calculator3.Function = 'bulk_node_ids'
calculator3.ResultArrayType = 'Unsigned Long'
UpdatePipeline(time=0.0, proxy=calculator3)

# create a new 'PassArrays'
passArrays2 = PassArrays(registrationName='PassArrays2',Input=calculator3)
passArrays2.PointDataArrays = ['bulk_node_ids', 'elevation', 'NodeArea']
UpdatePipeline(time=0.0, proxy=passArrays2)

# save data
SaveData(vtu_river_file_name_out, proxy=passArrays2, DataMode='Binary')
#print (vtu_file_name_out)



