import sys
import os
from vtk import *
##########################################################################################
# files and parameters
realtive_input_file_path = str(sys.argv[1])
realtive_ouput_file_path = str(sys.argv[2])
#file_path = os.path.abspath(os.path.dirname(__file__))
file_path = os.getcwd()
#print (file_path)
vtu_file_name = file_path + '/' + realtive_input_file_path
vtu_file_name_out = file_path + '/' + realtive_ouput_file_path

print ('Converting ASCII into BINARY: ')    
print (vtu_file_name)

##########################################################################################
reader = vtkXMLUnstructuredGridReader() #VTU Reader
reader.SetFileName(vtu_file_name)
reader.Update() # Needed because of GetScalarRange
mesh = reader.GetOutput()
scalar_range = mesh.GetScalarRange()
cellIds = vtkIdList()
##########################################################################################
extract = vtkExtractUnstructuredGridPiece()
extract.SetInputConnection(reader.GetOutputPort())
cellData = mesh.GetCellData()
writer = vtkXMLUnstructuredGridWriter()
writer.SetFileName(vtu_file_name_out)
writer.SetDataModeToBinary()
writer.SetInputConnection(extract.GetOutputPort())
writer.Write()


