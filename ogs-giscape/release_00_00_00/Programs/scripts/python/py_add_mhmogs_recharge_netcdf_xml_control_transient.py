#!/usr/bin/env python
import sys
import xml.etree.ElementTree as ET

def write_reading_rasters_xml(nc_file_name, variable_name, number_of_timesteps, output_file):
    """Generates an XML file listing all raster files to be read."""
    xml_rasters = ET.Element("rasters")
    for number_of_timestep in range(number_of_timesteps + 1):
        raster = ET.SubElement(xml_rasters, "raster")
        ET.SubElement(raster, "file").text = nc_file_name
        ET.SubElement(raster, "variable").text = variable_name
        ET.SubElement(raster, "dimension").text = str(number_of_timestep)
    ET.indent(xml_rasters, space="    ", level=1)
    ET.ElementTree(xml_rasters).write(output_file)

def write_raster_parameter_xml(nc_file_name, variable_name, number_of_timesteps, output_file):
    """Creates an XML file for defining raster parameters and their scaled versions."""
    xml_parameters = ET.Element("parameters")
    for number_of_timestep in range(number_of_timesteps + 1):
        parameter = ET.SubElement(xml_parameters, "parameter")
        parameter_name = f"{nc_file_name.replace('.nc', '')}_{variable_name}_{number_of_timestep}"
        ET.SubElement(parameter, "name").text = parameter_name
        ET.SubElement(parameter, "type").text = 'Raster'
        scaled_parameter = ET.SubElement(xml_parameters, "parameter")
        ET.SubElement(scaled_parameter, "name").text = f"{parameter_name}_scaled"
        ET.SubElement(scaled_parameter, "type").text = 'CurveScaled'
        ET.SubElement(scaled_parameter, "curve").text = 'mHM2OGSRechargeUnits'
        ET.SubElement(scaled_parameter, "parameter").text = parameter_name
    ET.indent(xml_parameters, space="    ", level=1)
    ET.ElementTree(xml_parameters).write(output_file)

def write_timedependent_parameter_xml(nc_file_name, variable_name, number_of_timesteps, output_file, timestep_size):
    """Generates an XML file for time-dependent parameter configurations."""
    xml_parameter = ET.Element("parameter")
    ET.SubElement(xml_parameter, "name").text = 'TimeDependentHeterogeneous_Recharge'
    ET.SubElement(xml_parameter, "type").text = 'TimeDependentHeterogeneousParameter'
    time_series = ET.SubElement(xml_parameter, "time_series")
    for number_of_timestep in range(number_of_timesteps + 1):
        pair = ET.SubElement(time_series, "pair")
        ET.SubElement(pair, "time").text = str(timestep_size * number_of_timestep)
        ET.SubElement(pair, "parameter_name").text = f"{nc_file_name.replace('.nc', '')}_{variable_name}_{number_of_timestep}_scaled"
    ET.indent(xml_parameter, space="    ", level=1)
    ET.ElementTree(xml_parameter).write(output_file)

def main():
    if len(sys.argv) != 8:
        print("Usage: script.py <netcdf_file_name> <Transient_RasterRechargeXMLFile> <Transient_RasterRechargeParameters> <Transient_RasterRechargeParameterTimeSeries> <netcdf_variable_name> <number_of_timesteps> <timestep_size>")
        sys.exit(1)

    netcdf_file_name = sys.argv[1]
    Transient_RasterRechargeXMLFile = sys.argv[2]
    Transient_RasterRechargeParameters = sys.argv[3]
    Transient_RasterRechargeParameterTimeSeries = sys.argv[4]
    netcdf_variable_name = sys.argv[5]
    number_of_timesteps = int(sys.argv[6])
    timestep_size = int(sys.argv[7])

    write_reading_rasters_xml(netcdf_file_name, netcdf_variable_name, number_of_timesteps, Transient_RasterRechargeXMLFile)
    write_raster_parameter_xml(netcdf_file_name, netcdf_variable_name, number_of_timesteps, Transient_RasterRechargeParameters)
    write_timedependent_parameter_xml(netcdf_file_name, netcdf_variable_name, number_of_timesteps, Transient_RasterRechargeParameterTimeSeries, timestep_size)

if __name__ == "__main__":
    main()

