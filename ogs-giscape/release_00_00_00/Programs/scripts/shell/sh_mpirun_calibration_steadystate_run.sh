subfolder_name=${PWD##*/}          # to assign to a variable
model_domain_name=$2

number_of_partitions=$1
container_path=../../../Programs/bin/ogs_mpi.sif

python3 ../../../Programs/scripts/python/py_create_OGSmaterialXMLfromCSV.py ../MaterialData.csv 2 000_Media.xml

echo "#############################################################"
echo "# HPC - Parallel OpenMPI Run: $model_domain_name  "
echo "#############################################################"

#cp ${number_of_partitions}/*.bin .
#apptainer exec ${container_path} mpirun -np ${number_of_partitions} ogs -o ../../../OGS_Results/${model_domain_name}/SteadyState/${number_of_partitions} 000_ModelRun4Calibration.prj

cd ${number_of_partitions}
apptainer exec ../${container_path} mpirun -np ${number_of_partitions} ogs -o ../../../../OGS_Results/${model_domain_name}/SteadyState/${number_of_partitions} 000_ModelRun4Calibration.prj
cd ..

pvpython  ../../../Programs/scripts/pvpython/pvpy_vtu2csv.py ../../../OGS_Results/$model_domain_name/SteadyState/${number_of_partitions}/MOD_01_MonitoringWells_ts_10_t_315360000000_000000.pvtu ../../../OGS_Results/$model_domain_name/SteadyState/${number_of_partitions}/PEST_OBS_SimResults.csv
pvpython  ../../../Programs/scripts/pvpython/pvpy_vtu2csv_baseflow.py ../../../OGS_Results/$model_domain_name/SteadyState/${number_of_partitions}/MOD_01_BC_RiverLakes_Deutschland_ts_10_t_315360000000_000000.pvtu ../../../OGS_Results/$model_domain_name/SteadyState/${number_of_partitions}/PEST_BF_SimResults.csv

