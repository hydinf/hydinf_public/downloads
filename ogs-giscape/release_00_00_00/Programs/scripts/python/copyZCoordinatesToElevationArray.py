#!/usr/bin/env python3

from vtk import *
from vtk.util import numpy_support
from sys import argv, exit

import VtkUtils

def copyZCoordsToElevationArray(mesh):
    """Copies the z coordinates of points into elevation array of the mesh and
    returns the mesh

    The function creates an array 'elevation' that contains the copies the z
    coordinates of the mesh points. The array is added to the point data of the
    mesh, which is returned by the function.
    """
    point_data = mesh.GetPointData()
    points = mesh.GetPoints().GetData()
    ncoords = numpy_support.vtk_to_numpy(points)

    elevation = vtkDoubleArray()
    elevation.SetNumberOfComponents(1)
    elevation.SetNumberOfTuples(mesh.GetNumberOfPoints())
    elevation.SetName("elevation")

    for i in range(0, mesh.GetNumberOfPoints()):
        elevation.SetValue(i, ncoords[i][2])
    ncoords[:, 2] = 0.0

    point_data.AddArray(elevation)
    return mesh


def main(input_file_name, output_file_name):
    mesh = VtkUtils.readVtkMesh(input_file_name)
    copyZCoordsToElevationArray(mesh)
    VtkUtils.writeVtkMesh(output_file_name, mesh)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: " + str(sys.argv[0]) + " input.vtu output.vtu")
        exit(1)

    main(sys.argv[1], sys.argv[2])
