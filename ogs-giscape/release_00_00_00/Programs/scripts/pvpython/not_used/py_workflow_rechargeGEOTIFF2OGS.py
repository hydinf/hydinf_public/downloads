# mHM-IMPORT by Thomas Kalbacher and Johannes Boog, UFZ, Leipzig, Germany
##########################################################################################
import sys
import os
from datetime import datetime
from vtk import *
from paraview.simple import *
from paraview import servermanager as sm
import numpy as np
##########################################################################################
# files and parameter specification
#-----------------------------------------------------------------------------------------
# files and parameters
recharge_geotiff_path = str(sys.argv[1])
surface_vtu_path = str(sys.argv[2])
output_vtu_path = str(sys.argv[3])

file_path = os.getcwd()
print (file_path)

geotiff_file_name = file_path + '/' + recharge_geotiff_path
vtu_input_filename = file_path + '/' + surface_vtu_path
save_as_file_name = file_path + '/' + output_vtu_path 

print('########################################################')
print('START')
print('########################################################')
print('FILE 1= ' + str(geotiff_file_name))
print('FILE 2= ' + str(vtu_input_filename))
print('FILE 3= ' + str(save_as_file_name))
print('########################################################')

print('. . . Reading Extracted OGS Surface VTU')
# create a new 'XML Unstructured Grid Reader'
rechargeSurfacevtu = XMLUnstructuredGridReader(registrationName='RechargeSurface', FileName=[vtu_input_filename])
#rechargeSurfacevtu.CellArrayStatus = ['bulk_element_ids', 'bulk_face_ids']
#rechargeSurfacevtu.PointArrayStatus = ['bulk_node_ids']

#Size of VTU
rechargeSurfacevtu.UpdatePipeline()
bounds = rechargeSurfacevtu.GetDataInformation().GetBounds()
x_min_vtu = bounds[0]
x_max_vtu = bounds[1]
y_min_vtu = bounds[2]
y_max_vtu = bounds[3]

print('########################################################')
print('. . . Clipping GEOTIFF')
print (x_min_vtu)
print (x_max_vtu)
print (y_min_vtu)
print (y_max_vtu)
add_frame = 10000 
local_geotiff_file_name = (str(save_as_file_name) [0:-3] + "tif") 
print (local_geotiff_file_name)
os.system("gdal_translate -projwin " + str(x_min_vtu-add_frame) + " " + str(y_max_vtu+add_frame) + " " + str(x_max_vtu+add_frame) + " " + str(y_min_vtu-add_frame) + " -of GTiff -ot Float64 " + str(geotiff_file_name) + " " + str(local_geotiff_file_name))

print('########################################################')
print('. . . Reading new GEOTIFF')
# create a new 'GDAL Raster Reader'
recharge_input_tif = GDALRasterReader(registrationName='recharge_input.tif', FileName= local_geotiff_file_name)
# Properties modified on recharge_yearly_dailymean__mm_per_daytif
recharge_input_tif.CollateBands = 0

print('########################################################')
print('. . . clean to grid')
# create a new 'Clean to Grid'
cleantoGrid1 = CleantoGrid(registrationName='CleantoGrid1', Input=recharge_input_tif)
cleantoGrid1.UpdatePipeline()
bounds = cleantoGrid1.GetDataInformation().GetBounds()
x_min_netcdf = bounds[0]
x_max_netcdf = bounds[1]
y_min_netcdf = bounds[2]
y_max_netcdf = bounds[3]

print('########################################################')
print ("x_min_vtu = " + str(x_min_vtu) + " | x_min_netcdf = " + str(x_min_netcdf) )
print ("y_min_vtu = " + str(y_min_vtu) + " | y_min_netcdf = " + str(y_min_netcdf) )
print ("x_max_vtu = " + str(x_max_vtu) + " | x_max_netcdf = " + str(x_max_netcdf) )
print ("y_max_vtu = " + str(y_max_vtu) + " | x_max_netcdf = " + str(y_max_netcdf) )



print('########################################################')
print('. . . clipping')
# Clip Grid to DataSet with 
clip1 = Clip(registrationName='Clip1', Input=cleantoGrid1)
clip1.ClipType = 'Box'
clip1.ClipType.Position = [x_min_vtu, y_min_vtu, -10000.00]
clip1.ClipType.Length = [(x_max_vtu-x_min_vtu), (y_max_vtu-y_min_vtu), 20000.00]


print('########################################################')
print('. . . add elelevation')
# create a new 'Calculator - Elevation'
calculator0 = Calculator(registrationName='Calculator1', Input=rechargeSurfacevtu)
calculator0.ResultArrayName = 'elevation'
calculator0.Function = 'coordsZ'
UpdatePipeline(time=0.0, proxy=calculator0)



print('########################################################')
print('. . . transform')
# create a new 'Transform'
transform1 = Transform(registrationName='Transform1', Input=calculator0)
# Properties modified on transform1.Transform
transform1.Transform.Scale = [1.0, 1.0, 0.0]

print('########################################################')
print('. . . resample')
# create a new 'Resample With Dataset'
resampleWithDataset1 = ResampleWithDataset(registrationName='ResampleWithDataset1', SourceDataArrays=clip1, DestinationMesh=transform1)

print('########################################################')
print('. . . point to cell data')
# create a new 'Point Data to Cell Data'
pointDatatoCellData1 = PointDatatoCellData(registrationName='PointDatatoCellData1', Input=resampleWithDataset1)

print('########################################################')
print('. . . append atributes')
# create a new 'Append Attributes'
appendAttributes1 = AppendAttributes(registrationName='AppendAttributes1', Input=[pointDatatoCellData1, transform1])

print('########################################################')
print('. . . clean to grid')
# create a new 'Clean to Grid'
cleantoGrid1 = CleantoGrid(registrationName='CleantoGrid2', Input=appendAttributes1)
#UpdatePipeline(time=0.0, proxy=cleantoGrid1)

print('########################################################')
print('. . . remove ghosts')
removeGhostInformation1 = RemoveGhostInformation(registrationName='RemoveGhostInformation1', Input=cleantoGrid1)

print('########################################################')
print('. . . remove whitespace in names')
# create a new 'Programmable Filter'
programmableFilter1 = ProgrammableFilter(registrationName='ProgrammableFilter1', Input=removeGhostInformation1)
# Properties modified on programmableFilter1
programmableFilter1.Script = """id = self.GetInput()
od = self.GetOutput()
od.ShallowCopy(id)
numCells = od.GetCellData().GetNumberOfArrays()
for n in range(od.GetCellData().GetNumberOfArrays()):
   old_name = od.GetCellData().GetArray(n).GetName()
   old_name_intro = old_name[0:5]
   if old_name_intro == "Band ":
     new_name = "Band" + str(old_name[5:10])
     od.GetCellData().GetArray(old_name).SetName(new_name)
     #print (new_name)   
"""
#programmableFilter1.RequestInformationScript = ''
#programmableFilter1.RequestUpdateExtentScript = ''
#programmableFilter1.PythonPath = ''


print('########################################################')
print ('Saving File: ' + str (save_as_file_name))
# save data
SaveData(save_as_file_name, proxy=programmableFilter1,DataMode='Appended', EncodeAppendedData=1, CompressorType='ZLib', CompressionLevel='5', )


print('########################################################')



