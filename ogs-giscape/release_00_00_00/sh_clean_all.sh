
#rm ./OGS/*.vtu
#rm ./OGS/*.mesh
#rm ./OGS/*.bin

rm -rf ./DataSets
mkdir ./DataSets

rm -rf ./OGS_Setups
mkdir ./OGS_Setups

rm -rf ./OGS_Results
mkdir ./OGS_Results

rm -r .snakemake/

