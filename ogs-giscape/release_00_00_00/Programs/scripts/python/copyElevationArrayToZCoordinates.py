#!/usr/bin/env python3

from vtk import *
from vtk.util import numpy_support
from sys import argv, exit

import VtkUtils

def copyElevationArrayToZCoords(mesh):
    """Copies the elevation array to z coordinates of points of the mesh and
    returns the mesh

    The function requires an array 'elevation'. The elevation entries where
    copied to the z coordinates of the mesh points.
    """
    point_data = mesh.GetPointData()
    points = mesh.GetPoints().GetData()
    ncoords = numpy_support.vtk_to_numpy(points)

    elevation = point_data.GetArray("elevation")

    for i in range(0, mesh.GetNumberOfPoints()):
        ncoords[i][2] = elevation.GetValue(i)

    return mesh


def main(input_file_name, output_file_name):
    mesh = VtkUtils.readVtkMesh(input_file_name)
    copyElevationArrayToZCoords(mesh)
    VtkUtils.writeVtkMesh(output_file_name, mesh)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: " + str(sys.argv[0]) + " input.vtu output.vtu")
        exit(1)

    main(sys.argv[1], sys.argv[2])
