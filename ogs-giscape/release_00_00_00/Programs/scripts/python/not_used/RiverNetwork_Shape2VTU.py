# by Thomas Kalbacher

print ('------ START SCRIPT ------')
#import required packages
import os
import sys
import time
import shutil
import itertools
import numpy as np
import pyvista as pv
import geopandas as gpd

##########################################################################################
# The source file
file_name = "GIS_Rivers_Data/RiversPoints.geojson"
try:
    shutil.rmtree('GMSH/Output')
except:
    os.mkdir('GMSH/Output')
try:
    os.stat('GMSH/Output')
except:
    os.mkdir('GMSH/Output')


    
lineDf = gpd.read_file(file_name)
#create emtpy dict to store the partial unstructure grids
lineTubes = {}
print(lineDf.crs)
print (lineDf)
print ("processing " + str(lineDf.shape[0]) + " rows")
printcounter = 1000

for index, values in lineDf.iterrows():
    cellSec = []
    linePointSec = []
    
    if (printcounter == 1000):
    	print (lineDf.shape[0]-index)
    	printcounter = 0
    printcounter +=1

    #iterate over the geometry coords
    zipObject = zip(values.geometry.xy[0],values.geometry.xy[1],itertools.repeat(values.elevation))
    
    for linePoint in zipObject:
        linePointSec.append([linePoint[0],linePoint[1],linePoint[2]])

    #get the number of vertex from the line and create the cell sequence
    nPoints = len(list(lineDf.loc[index].geometry.coords))
    cellSec = [nPoints] + [i for i in range(nPoints)]

    #convert list to numpy arrays
    cellSecArray = np.array(cellSec)
    cellTypeArray =+ np.array([4])
    linePointArray =+ np.array(linePointSec)
    partialLineUgrid = pv.UnstructuredGrid(cellSecArray,cellTypeArray,linePointArray)   
    lineTubes[str(index)] = partialLineUgrid
    partialLineUgrid.save('./GMSH/Output/2D_RiverGeometry_'+str(index)+'.vtu',binary=False)
print (" . . . done ")


print ('Number of points or lines: ' + str(index))
#merge all tubes and export resulting vtk
#lineBlocks = pv.MultiBlock(lineTubes)
#lineGrid = lineBlocks.combine()
#lineGrid.save('2D_DomainGeometry.vtu',binary=True)
#partialLineUgrid.save('2D_DomainGeometry.vtu',binary=False)


print ('------ END SCRIPT ------')

