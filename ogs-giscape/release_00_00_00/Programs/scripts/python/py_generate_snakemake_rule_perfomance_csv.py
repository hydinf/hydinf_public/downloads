import sys
import csv
import glob
import os
from datetime import datetime
import pandas as pd
import matplotlib.pyplot as plt

def process_benchmarks(benchmarks_dir):
    """
    Process benchmark files in the specified directory and generate a summary CSV file.
    
    Parameters:
        benchmarks_dir (str): Path to the directory containing the .bench files.
        
    Returns:
        str: Path to the generated CSV file.
    """
    # Path to the CSV file that will be created
    output_csv_path = os.path.join(benchmarks_dir, 'snakemake_rule_perfomance.csv')

    # Initialize a list to collect the data
    data = []

    # Update the fieldnames to include file modification time
    fieldnames = ['rule_name', 'file_modification_time', 's', 'h:m:s', 'max_rss', 'max_vms', 'max_uss', 'max_pss', 'io_in', 'io_out', 'mean_load', 'cpu_time']

    # Iterate over the directory for .bench files
    for bench_file in glob.glob(os.path.join(benchmarks_dir, '*.bench')):
        # Extract the rule name from the file name by removing '.bench'
        rule_name = os.path.basename(bench_file).replace('.bench', '')
        # Get the modification time of the file and convert it to a human-readable format
        mod_time = datetime.fromtimestamp(os.path.getmtime(bench_file)).strftime('%Y-%m-%d %H:%M:%S')
        with open(bench_file, 'r') as file:
            # Skip the header line
            next(file)
            # Read the data row
            reader = csv.reader(file, delimiter='\t')
            for row in reader:
                # Combine the rule name and modification time with the data from the row
                row_data = {fieldnames[i]: value for i, value in enumerate([rule_name, mod_time] + row, start=0)}
                data.append(row_data)

    # Write the collected data to a CSV file
    with open(output_csv_path, 'w', newline='') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        for row in data:
            writer.writerow(row)

    print(f"Summary of benchmarks has been saved to {output_csv_path}.")

    # Delete all .bench files
    for bench_file in glob.glob(os.path.join(benchmarks_dir, '*.bench')):
        os.remove(bench_file)

    return output_csv_path

def generate_plots(csv_file_path, benchmarks_dir):
    """
    Generate plots based on the benchmark summary CSV file.
    
    Parameters:
        csv_file_path (str): Path to the benchmark summary CSV file.
        benchmarks_dir (str): Path to the directory containing the .bench files.
    """
    # Read the benchmark summary from the CSV file
    df = pd.read_csv(csv_file_path)

    # Convert the 's' (seconds) and 'max_rss' (maximum resident set size) values to numeric values for calculations
    df['s'] = pd.to_numeric(df['s'], errors='coerce')
    df['max_rss'] = pd.to_numeric(df['max_rss'], errors='coerce')

    # Calculate the total time and memory used by all rules
    total_time = df['s'].sum()
    total_memory = df['max_rss'].sum()

    # Calculate the percentage of the total time and memory each rule uses
    df['time_percentage'] = (df['s'] / total_time) * 100
    df['memory_percentage'] = (df['max_rss'] / total_memory) * 100

    # Group the data by rule name and sum the percentages to get the total share for each rule
    time_percentage = df.groupby('rule_name')['time_percentage'].sum()
    memory_percentage = df.groupby('rule_name')['memory_percentage'].sum()

    # Also, calculate the total time in seconds and total memory usage in MB for each rule
    total_time_by_rule = df.groupby('rule_name')['s'].sum()
    total_memory_by_rule = df.groupby('rule_name')['max_rss'].sum()

    # Plot for total time percentage usage
    if not time_percentage.empty:
        plt.figure(figsize=(10, 8))
        time_percentage.sort_values().plot(kind='barh', color='skyblue')
        plt.xlabel('Percent of Total Time')
        plt.ylabel('Rule')
        plt.title('Share of Each Rule in Total Runtime')
        plt.tight_layout()
        plt.savefig(os.path.join(benchmarks_dir, 'percentage_time_usage_plot.png'))
        plt.clf()
    else:
        print("No valid data for 'time_percentage' available.")

    # Plot for absolute total time usage in seconds
    if not total_time_by_rule.empty:
        total_time_by_rule.sort_values().plot(kind='barh', figsize=(10, 8), color='coral')
        plt.xlabel('Total Time in Seconds')
        plt.ylabel('Rule')
        plt.title('Absolute Total Runtime for Each Rule')
        plt.tight_layout()
        plt.savefig(os.path.join(benchmarks_dir, 'absolute_time_usage_plot.png'))
        plt.clf()
    else:
        print("No valid data for 'total_time_by_rule' available.")

    # Plot for absolute total memory usage in MB
    if not total_memory_by_rule.empty:
        total_memory_by_rule.sort_values().plot(kind='barh', figsize=(10, 8), color='purple')
        plt.xlabel('Total Memory Usage in MB')
        plt.ylabel('Rule')
        plt.title('Absolute Total Memory Usage for Each Rule')
        plt.tight_layout()
        plt.savefig(os.path.join(benchmarks_dir, 'absolute_memory_usage_plot.png'))
        plt.clf()
    else:
        print("No valid data for 'total_memory_by_rule' available.")

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python script.py <benchmarks_dir>")
    else:
        benchmarks_dir = sys.argv[1]
        csv_file_path = process_benchmarks(benchmarks_dir)
        generate_plots(csv_file_path, benchmarks_dir)
