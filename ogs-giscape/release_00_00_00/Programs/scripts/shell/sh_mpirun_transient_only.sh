subfolder_name=${PWD##*/}          # to assign to a variable
subfolder_name=${subfolder_name:-/}        # to correct for the case where PWD=/

#!/usr/bin/env bash
# values to change
MESH_BASENAME=Subsurface3D_IC
MESH=${MESH_BASENAME}.vtu
BC_MESHES='BC_RiverLakes_Deutschland.vtu OUT_Recharge.vtu OUT_Rivers.vtu Surface3D.vtu'
# end values to change
container_path=../../../Programs/bin/ogs_mpi.sif

number_of_partitions=$1
model_domain_name=$2

# Identify Subdomains to ensure matching node and element IDs
apptainer exec ${container_path} RemoveGhostData -i ../../../OGS_Results/${model_domain_name}/SteadyState/${number_of_partitions}/MOD_01_Subsurface3D_ts_10_t_315360000000_000000.pvtu  -o Subsurface3D_IC.vtu


apptainer exec ${container_path} identifySubdomains -f -m Subsurface3D_IC.vtu -s 0.1 BC_RiverLakes_Deutschland.vtu
apptainer exec ${container_path} identifySubdomains -f -m Subsurface3D_IC.vtu -s 0.1 OUT_Recharge.vtu
apptainer exec ${container_path} identifySubdomains -f -m Subsurface3D_IC.vtu -s 0.1 OUT_Rivers.vtu
apptainer exec ${container_path} identifySubdomains -f -m Subsurface3D_IC.vtu -s 0.1 Surface3D.vtu


if [ ! -d ${number_of_partitions} ];
then
    mkdir ${number_of_partitions}
fi

if [ ! -e ${MESH_BASENAME}.mesh ];
then
    apptainer exec ${container_path} partmesh --ogs2metis -i ${MESH}
fi

if [ ! -e ${number_of_partitions}/${MESH_BASENAME}.mesh ];
then
    cd ${number_of_partitions}
    #ln -s ../${MESH_BASENAME}.mesh ${MESH_BASENAME}.mesh
    cp ../${MESH_BASENAME}.mesh ${MESH_BASENAME}.mesh
    cd ..
fi

apptainer exec ${container_path} partmesh -m -n ${number_of_partitions} -i ${MESH} -o ${number_of_partitions} -- ${BC_MESHES}

echo "#############################################################"
echo "# Transient HPC - Parallel OpenMPI Run: ${subfolder_name}    "
echo "#############################################################"

declare -a project_files=(
    '001_ModelRun.prj'
    '000_Media.xml'
    '001_TimeStepping.xml'
    '001_RasterRechargeParameterTimeSeries.xml'
    '001_RasterRechargeParameters.xml'
    '001_RasterRechargeXMLFile.xml'
    'BC_Recharge_Transient.nc'
)

cd ${number_of_partitions}
# create links for project files
for project_file in "${project_files[@]}";
do
    ln -s ../${project_file} ${project_file}
done


mkdir -p ../../../../OGS_Results/${model_domain_name}/Transient/${number_of_partitions}

cp ${number_of_partitions}/*.bin .
apptainer exec ../${container_path} mpirun -np ${number_of_partitions} ogs -o ../../../../OGS_Results/${model_domain_name}/Transient/${number_of_partitions} 001_ModelRun.prj




