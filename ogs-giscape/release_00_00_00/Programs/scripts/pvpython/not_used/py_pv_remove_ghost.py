#Clipping by Thomas Kalbacher, UFZ, Leipzig, Germany
##########################################################################################
import sys
import os
from datetime import datetime
from vtk import *
from paraview.simple import *
import numpy as np
##########################################################################################
# parameter specification from input arguments
realtive_path = str(sys.argv[1])
#-----------------------------------------------------------------------------------------
# files and parameters
file_path = os.getcwd()
#-----------------------------------------------------------------------------------------
vtu_input_filename = file_path + '/' + realtive_path
#file_name_base = 'output_of_all_timesteps_0'
save_as_file_name = vtu_input_filename

print('. . . Reading OGS-BC-VTU' +  str(save_as_file_name))
# create a new 'XML Unstructured Grid Reader'
DomainVTU = XMLUnstructuredGridReader(registrationName='Domain', FileName=[vtu_input_filename])
#DomainVTU.CellArrayStatus = ['MaterialIDs']
#rechargeSurfacevtu.PointArrayStatus = ['bulk_node_ids']
removeGhostInformation1 = RemoveGhostInformation(registrationName='RemoveGhostInformation1', Input=DomainVTU)
#Size of VTU
DomainVTU.UpdatePipeline()




print ('Saving File: ' + str (save_as_file_name))
# save data
SaveData(save_as_file_name, proxy=removeGhostInformation1,DataMode='Binary')



